# frozen_string_literal: true

require 'active_support'
require 'jwt'

class JsonWebToken
  class << self
    def encode(data)
      exp = ENV['JWT_EXPIRATION_DATE'].to_i.days.from_now
      payload = {
        data: data,
        exp: exp.to_i
      }
      JWT.encode payload, ENV['JWT_SECRET_KEY'], ENV['JWT_ALGORITHM']
    end

    def decode(token)
      body = JWT.decode(token, ENV['JWT_SECRET_KEY'])[0]
      HashWithIndifferentAccess.new body
    rescue StandardError
      nil
    end
  end
end
