# frozen_string_literal: true

require 'image_processing/mini_magick'

class ImageUploader < Shrine
  Attacher.validate do
    validate_max_size 10 * 1024 * 1024
    validate_mime_type %w[image/jpeg image/png]
  end
  Attacher.derivatives do |original|
    magick = ImageProcessing::MiniMagick.source(original)
    {
      large: magick.resize_to_limit!(1200, 1200),
      medium: magick.resize_to_limit!(640, 640),
      small: magick.resize_to_limit!(180, 180)
    }
  end
end
