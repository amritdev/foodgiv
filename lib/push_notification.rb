# frozen_string_literal: true

require 'rpush'

class PushNotification
  class << self
    def create_fcm_notification(user:, notification:, data: nil, priority: 'high', content_available: false, collapse_key: '')
      @user = user
      return if @user.nil?

      Rpush::Gcm::Notification.new(
        app: Rpush::Gcm::App.where(name: 'android_app').first,
        registration_ids: [get_device_token],
        data: data,
        priority: priority,
        content_available: content_available,
        collapse_key: collapse_key,
        notification: notification
      ).save!
    end

    def get_device_token
      return nil if @user.device_info.blank?

      @user.device_info['device_token']
    end
  end
end
