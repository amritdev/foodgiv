# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module App
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    # ActiveRecord::Base.logger = Logger.new STDOUT
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: %i[get post options]
      end
    end
    # Read env file
    Dotenv.load
    # Autoload lib files
    config.autoload_paths << Rails.root.join('lib')
    config.autoload_paths << Rails.root.join('app/graphql/queries')
    config.autoload_paths << Rails.root.join('app/graphql/loaders')
    config.eager_load_paths << Rails.root.join('lib')
  end
end
