# frozen_string_literal: true

Rails.application.configure do
  config.twilio = ActiveSupport::OrderedOptions.new
  constants = Rails.application.config_for :application
  config.twilio.account_sid = constants['twilio']['account_sid']
  config.twilio.auth_token = constants['twilio']['auth_token']
  config.twilio.number = constants['twilio']['number']
  config.google = ActiveSupport::OrderedOptions.new
  config.google.geocode_api_key = constants['google']['geocode_api_key']
  config.google.oauth_web_client_id = constants['google']['oauth_web_client_id']
  config.google.oauth_android_client_id = constants['google']['oauth_android_client_id']
  config.google.oauth_ios_client_id = constants['google']['oauth_ios_client_id']
  config.stripe = ActiveSupport::OrderedOptions.new
  config.stripe.publishable_key = constants['stripe']['publishable_key']
  config.stripe.secret_key = constants['stripe']['secret_key']
  config.stripe.price_id = constants['stripe']['price_id']
  config.rpush = ActiveSupport::OrderedOptions.new
  config.rpush.fcm_server_key = constants['rpush']['fcm_server_key']
end
