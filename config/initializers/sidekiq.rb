# frozen_string_literal: true
require 'sidekiq/web'

sidekiq_redis_config = {
  url: ENV['REDIS_URL'],
  password: ENV['REDIS_PASSWORD']
}

Sidekiq.configure_server do |config|
  config.redis = sidekiq_redis_config
  config.on(:startup) do
    SidekiqScheduler::Scheduler.instance.reload_schedule!
  end
end

Sidekiq.configure_client do |config|
  config.redis = sidekiq_redis_config
end

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  Rack::Utils.secure_compare(::Digest::SHA256.hexdigest(user), ::Digest::SHA256.hexdigest(ENV["SIDEKIQ_DASHBOARD_USER"])) &
  Rack::Utils.secure_compare(::Digest::SHA256.hexdigest(password), ::Digest::SHA256.hexdigest(ENV["SIDEKIQ_DASHBOARD_PASSWORD"]))
end
