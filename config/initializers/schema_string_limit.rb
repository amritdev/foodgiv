# frozen_string_literal: true

require 'active_record/connection_adapters/mysql2_adapter'
ActiveRecord::ConnectionAdapters::AbstractMysqlAdapter::NATIVE_DATABASE_TYPES[:string] = {
  name: 'varchar',
  limit: 191
}
