# Overview

This repository holds source code of foodgiv backend server, which is built on Ruby on Rails framework.

## Run

Start the server with docker compose:

```console
$ docker-compose up
```

## Development

If you want to execute a command on server (rails container) use the following shell command:

```console
$ docker exec -it api {YOUR_COMMAND}
```

For example, if you want to run test in docker:

```console
$ docker exec -it api rails test
```

If you want a shell access:

```console
$ docker exec -it api /bin/bash
```

For db shell access:

```console
$ docker exec -it mysql /bin/bash
# mysql -u root -p
```

## URLs

- Server API: http://localhost:3001
- GraphQL Endpoint: http://localhost:3001/graphql
- GraphQL Sandbox: http://localhost:3001/graphiql
- Sidekiq Dashboard: http://localhost:3001/sidekiq
- Redis Commander: http://localhost:8082

### Troubleshooting

#### Rails cannot connect to mysql

Since docker compose does not create user automatically from `docker-compose.yml`, you have to create a user manually like the following:

```console
$ docker exec -it mysql /bin/bash

// mysql server
# mysql -u root -p
mysql> CREATE USER 'foodgiv-dbadmin'@'%' IDENTIFIED BY 'password';GRANT ALL PRIVILEGES ON *.* TO 'foodgiv-dbadmin'@'%' WITH GRANT OPTION;FLUSH PRIVILEGES;
```

#### Redis::CannotConnectError (Error connecting to Redis on 127.0.0.1:6379 (Errno::EADDRNOTAVAIL))

`REDIS_HOST` in `.env` file should be docker host machine address, not 127.0.0.1

#### Rails GraphQL shows error in Windows 10

```console
$ cd {PROJECT_ROOT}/tmp/cache/assets/sprockets
$ fsutil.exe file SetCaseSensitiveInfo ./v3.0 enable
```

#### bin/rails: Permission denied

```console
$ chmod u+x bin/rails
```
#### Failed opening the RDB file dump.rdb (in server root dir /etc/crontabs) for saving

```console
$ docker exec -it {CONTAINER_ID} bash

// redis shell
$ redis-cli -a {PASSWORD}

// redis cli
> CONFIG SET DIR /data
```

## Test

```console
$ rspec spec --format documentation
```
