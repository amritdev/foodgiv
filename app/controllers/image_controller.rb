# frozen_string_literal: true

class ImageController < ApplicationController
  include ImageHelper
  before_action :authenticate
  rescue_from ::StandardError, with: :handle_error

  def new
    image = Image.new(new_params)
    image.image_derivatives!
    image.save!
    render json: {
      success: true,
      data: to_hash(image)
    }
  end

  def multiple_upload
    images_data = multiple_upload_params
    images = []
    images_data[:images].values.slice(0, 10).each do |image_data|
      image = Image.new(image: image_data)
      image.image_derivatives!
      image.save!
      images << to_hash(image)
    end
    render json: {
      success: true,
      data: images
    }
  end

  def handle_error(exception)
    render json: {
      success: false,
      data: exception
    }
    raise exception
  end
end
