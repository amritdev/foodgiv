# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include AuthHelper
  include ActionController::MimeResponds

  protect_from_forgery with: :null_session
  skip_forgery_protection

  def authenticate
    current_user = get_current_user_from(headers: request.headers)
    unless current_user
      render json: {
        success: false,
        msg: 'AUTHENTICATION_FAILED'
      }
    end
  end
end
