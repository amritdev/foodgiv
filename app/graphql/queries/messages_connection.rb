# frozen_string_literal: true

module MessagesConnection
  extend ActiveSupport::Concern

  included do
    field :messages_connection, Types::MessageConnectionType, null: false, max_page_size: 10 do
      description 'Get messages pagination'
      argument :filter, Types::MessageConnectionFilterType, required: false
    end
  end

  def messages_connection(input = { filter: { request_id: nil, show_archived: nil } })
    auth_check
    user = context[:current_user]
    filter = input[:filter]
    scope = Message.where_belongs_to user
    scope = scope.where(request_id: filter[:request_id]) if filter[:request_id].present?
    scope = scope.unarchived user unless filter[:show_archived]
    scope = scope.order(created_at: :desc, id: :desc)
    scope.uniq
  end
end
