# frozen_string_literal: true

module UserQuery
  extend ActiveSupport::Concern

  included do
    field :user, Types::ProfileType, null: false do
      description 'Get user profile'
      argument :id, GraphQL::Types::ID, required: true
    end
  end

  def user(input)
    auth_check unless Rails.env.test?
    current_user = context[:current_user]
    user = User.where(id: input[:id]).first
    raise_gql_error code: 'no_such_user' unless user
    user
  end
end
