# frozen_string_literal: true

module FoodQuery
  extend ActiveSupport::Concern

  included do
    field :food, Types::FoodType, null: false do
      description 'Get food by id'
      argument :id, GraphQL::Types::ID, required: true
    end
  end

  def food(input = { id: nil })
    auth_check
    current_user = context[:current_user]
    food = Food.where(id: input[:id]).first
    raise_gql_error code: 'no_such_food' unless food
    if food.status == 'UNLISTED' && food.user_id != current_user.id
      raise_gql_error code: 'no_such_food'
    end
    food
  end
end
