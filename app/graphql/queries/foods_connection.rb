# frozen_string_literal: true

module FoodsConnection
  extend ActiveSupport::Concern

  included do
    field :foods_connection, Types::FoodConnectionType, null: false, max_page_size: 50 do
      description 'Get food listing pagination'
      argument :filter, Types::FoodConnectionFilterType, required: false
      argument :sort, Types::FoodConnectionSortEnum, required: false
    end
  end

  def foods_connection(input = { filter: nil, sort: nil })
    filter = input[:filter]
    sort = input[:sort]
    scope = Food.listed.joins(:location)
    scope = scope.left_outer_joins(:labels) if filter && filter[:labels_ids]
    scope = scope.left_outer_joins(:allergens) if filter && filter[:allergens_ids]

    if sort && sort == 'CLOSEST'
      if filter && filter[:address].present?
        scope = scope.by_distance(origin: filter[:address])
      else
        user = context[:current_user]
        scope = scope.by_distance(origin: user.location) if user&.location
      end
    end

    scope = scope.order(created_at: :desc, id: :desc)

    if filter
      # filter by food type
      scope = scope.where(food_type: filter[:food_type]) if filter[:food_type]
      # filter by labels
      scope = scope.where('labels.id IN (?)', filter[:labels_ids]) if filter[:labels_ids].present?
      # filter by allergens
      if filter[:allergens_ids].present?
        scope = scope.where('allergens.id IS NULL OR allergens.id NOT IN (?)', filter[:allergens_ids])
      end
      # filter by boundary
      if filter[:boundary].present?
        sw = Geokit::LatLng.new(filter[:boundary][:south_west][:lat], filter[:boundary][:south_west][:lng])
        ne = Geokit::LatLng.new(filter[:boundary][:north_east][:lat], filter[:boundary][:north_east][:lng])
        origin = Geokit::LatLng.new(filter[:boundary][:origin][:lat], filter[:boundary][:origin][:lng])
        scope = scope.in_bounds([sw, ne], origin: origin)
      else
        # filter by distance and geolocation
        max_distance = filter[:distance]
        if max_distance.present? && max_distance > 0
          scope = scope.within(max_distance, origin: filter[:address]) if filter[:address].present?
          if filter[:geolocation].present?
            scope = scope.within(max_distance, origin: [filter[:geolocation][:lat], filter[:geolocation][:lng]])
          end
        end
      end
      # filter by keyword
      if filter[:keyword].present? && !filter[:keyword].empty?
        query_param = "%#{filter[:keyword].downcase}%"
        scope = scope.where('LCASE(foods.title) LIKE ? OR LCASE(foods.description) LIKE ? OR LCASE(locations.formatted_address) LIKE ?', query_param, query_param, query_param)
      end
    end

    scope.uniq
  end
end
