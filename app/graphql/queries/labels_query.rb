# frozen_string_literal: true

module LabelsQuery
  extend ActiveSupport::Concern

  included do
    field :labels, [Types::LabelType], null: false do
      description 'Get all food labels'
    end
  end

  def labels
    Rails.cache.fetch('labels', expires_in: 24.hours) do
      Label.all.load
    end
  end
end
