# frozen_string_literal: true

module CurrentUserQuery
  extend ActiveSupport::Concern

  included do
    field :current_user, Types::UserType, null: true do
      description 'Get current user by jwt token'
      argument :token, String, required: true
    end
  end

  def current_user(input)
    token = input[:token]
    user = if token
             user_id = get_decoded_auth_token token
             begin
               user_id ? User.find(user_id) : nil
             rescue StandardError
               nil
             end
            end
    save_login_info user if user
    user
  end
end
