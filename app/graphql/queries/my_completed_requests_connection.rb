# frozen_string_literal: true

module MyCompletedRequestsConnection
  extend ActiveSupport::Concern

  included do
    field :my_completed_requests_connection, Types::RequestConnectionType, null: false, max_page_size: 10 do
      description 'Return completed requests that belong to user'
      argument :type, Types::CompletedRequestFilterEnum, required: false
    end
  end

  def my_completed_requests_connection(input = { type: nil })
    auth_check
    user = context[:current_user]
    scope = Request.includes(:review_give, :review_receive)
    scope = if input[:type].present?
              case input[:type]
              when 'GIVE'
                scope.where(giver_id: user.id)
              when 'RECEIVE'
                scope.where(receiver_id: user.id)
              when 'REVIEW_NOT_CREATED'
                scope.where_belongs_to(user).where_no_review_by(user)
              when 'REVIEW_CREATED'
                scope.where_belongs_to(user).where_review_created_by(user)
              when 'REVIEW_RECEIVED'
                scope.where_belongs_to(user).where_review_received_by(user)
              else
                scope.where_belongs_to(user)
                      end
            else
              scope.where_belongs_to(user)
            end
    scope = scope.where(status: 'COMPLETED')
                 .uniq
                 .sort_by(&:last_status_changed_at)
                 .reverse
    scope
    end
end
