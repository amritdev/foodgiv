# frozen_string_literal: true

module AllergensQuery
  extend ActiveSupport::Concern

  included do
    field :allergens, [Types::AllergenType], null: false do
      description 'Get all food allergens'
    end
  end

  def allergens
    Rails.cache.fetch('allergens', expires_in: 24.hours) do
      Allergen.all.load
    end
  end
end
