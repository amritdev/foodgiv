# frozen_string_literal: true

module FoodRequestSent
  extend ActiveSupport::Concern

  included do
    field :food_request_sent, GraphQL::Types::Boolean, null: false do
      description 'Check if request is sent to food'
      argument :id, GraphQL::Types::ID, required: true
    end
  end

  def food_request_sent(input = { id: nil })
    auth_check
    current_user = context[:current_user]
    food = Food.where(id: input[:id]).first
    raise_gql_error code: 'no_such_food' unless food
    Request.where(food_id: food.id, receiver_id: current_user.id).count > 0
  end
end
