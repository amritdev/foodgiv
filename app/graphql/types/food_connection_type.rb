# frozen_string_literal: true

module Types
  class FoodTypeEnum < Types::BaseEnum
    value 'PREPARED'
    value 'UNPREPARED'
  end

  class FoodConnectionSortEnum < Types::BaseEnum
    value 'NEWEST'
    value 'CLOSEST'
  end

  class Geolocation < Types::BaseInputObject
    argument :lat, Float, required: true
    argument :lng, Float, required: true
  end

  class Boundary < Types::BaseInputObject
    argument :south_west, Geolocation, required: true
    argument :north_east, Geolocation, required: true
    argument :origin, Geolocation, required: true
  end

  class FoodConnectionFilterType < Types::BaseInputObject
    argument :food_type, Types::FoodTypeEnum, required: false
    argument :labels_ids, [Integer], required: false
    argument :allergens_ids, [Integer], required: false
    argument :distance, Integer, required: false
    argument :address, String, required: false
    argument :geolocation, Types::Geolocation, required: false
    argument :boundary, Types::Boundary, required: false
    argument :keyword, String, required: false
  end

  class FoodEdgeType < GraphQL::Types::Relay::BaseEdge
    node_type(Types::FoodType)
  end

  class FoodConnectionType < GraphQL::Types::Relay::BaseConnection
    field :total_count, Integer, null: false
    def total_count
      object.nodes.size
    end
    edge_type(FoodEdgeType)
  end

  class RequestFoodConnectionFilterType < Types::BaseInputObject
    argument :status, Types::FoodStatusEnum, required: false
  end
end
