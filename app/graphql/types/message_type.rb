# frozen_string_literal: true

module Types
  class MessageType < Types::BaseObject
    field :id, ID, null: false
    field :sender, UserPublicAttributeType, null: false
    field :receiver, UserPublicAttributeType, null: false
    field :request, RequestType, null: true
    field :food, FoodType, null: true
    field :content, String, null: false
    field :content_type, String, null: false
    field :read_at, String, null: true
    field :sender_archived, Boolean, null: false
    field :receiver_archived, Boolean, null: false
    field :created_at, String, null: false
    field :updated_at, String, null: true

    def sender
      RecordLoader.for(User).load(object.sender_id)
    end

    def receiver
      RecordLoader.for(User).load(object.receiver_id)
    end

    def request
      RecordLoader.for(Request).load(object.request_id)
    end

    def food
      RecordLoader.for(Food).load(object.food_id)
    end
  end
end
