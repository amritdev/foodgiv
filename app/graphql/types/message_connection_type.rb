# frozen_string_literal: true

module Types
  class MessageConnectionFilterType < Types::BaseInputObject
    argument :request_id, Integer, required: false
    argument :show_archived, Boolean, required: false
  end

  class MessageEdgeType < GraphQL::Types::Relay::BaseEdge
    node_type(Types::MessageType)
  end

  class MessageConnectionType < GraphQL::Types::Relay::BaseConnection
    field :total_count, Integer, null: false
    def total_count
      object.nodes.size
    end
    edge_type(MessageEdgeType)
  end
end
