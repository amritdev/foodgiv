# frozen_string_literal: true

module Types
  class RequestType < Types::BaseObject
    field :id, ID, null: false
    field :giver, UserPublicAttributeType, null: false
    field :receiver, UserPublicAttributeType, null: false
    field :food, FoodType, null: false
    field :message, String, null: true
    field :accepted_at, String, null: true
    field :declined_at, String, null: true
    field :cancelled_at, String, null: true
    field :completed_at, String, null: true
    field :read, Boolean, null: true
    field :status, String, null: false
    field :giver_archived, Boolean, null: true
    field :receiver_archived, Boolean, null: true
    field :archived, Boolean, null: true
    field :created_at, String, null: false
    field :last_action_at, String, null: false
    field :updated_at, String, null: true
    field :last_message, MessageType, null: true
    field :unread_message_count, Integer, null: false
    field :review_give, ReviewType, null: true
    field :review_receive, ReviewType, null: true

    def giver
      RecordLoader.for(User).load(object.giver_id)
    end

    def receiver
      RecordLoader.for(User).load(object.receiver_id)
    end

    def food
      RecordLoader.for(Food).load(object.food_id)
    end

    def last_message
      last_msg = object.last_message
      if last_msg.nil?
        nil
        # RecordLoader.for(Message).load(-1)
      else
        RecordLoader.for(Message).load(last_msg.id)
      end
    end

    def review_give
      review = object.review_give
      if review.nil?
        # RecordLoader.for(Review).load(-1)
        nil
      else
        RecordLoader.for(Review).load(review.id)
      end
    end

    def review_receive
      review = object.review_receive
      if review.nil?
        # RecordLoader.for(Review).load(-1)
        nil
      else
        RecordLoader.for(Review).load(review.id)
      end
    end

    def unread_message_count
      user = context[:current_user]
      return 0 unless user

      object.unread_message_count(user)
    end

    def archived
      user = context[:current_user]
      object.archived? user
    end
  end
end
