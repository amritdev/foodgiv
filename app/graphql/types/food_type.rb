# frozen_string_literal: true

module Types
  class FoodType < Types::BaseObject
    field :id, ID, null: false
    field :user, Types::UserPublicAttributeType, null: false
    field :title, String, null: false
    field :description, String, null: true
    field :labels, [LabelType], null: false
    field :allergens, [AllergenType], null: false
    field :food_type, FoodTypeEnum, null: false
    field :quantity, Integer, null: true
    field :pickup_time, String, null: true
    field :exp_date, String, null: true
    field :location, Types::LocationType, null: true
    field :status, Types::FoodStatusEnum, null: false
    field :images, [Types::ImageType], null: false
    field :request_count, Integer, null: false
    field :listed_at, String, null: true
    field :created_at, String, null: false
    field :updated_at, String, null: true

    def user
      RecordLoader.for(User).load(object.user_id)
    end

    def labels
      labels_ids = object.labels.pluck(:id).to_a
      RecordLoader.for(Label).load_many(labels_ids)
    end

    def allergens
      allergens_ids = object.allergens.pluck(:id).to_a
      RecordLoader.for(Allergen).load_many(allergens_ids)
    end

    def location
      RecordLoader.for(Location).load(object.location_id)
    end

    def images
      images_ids = object.images.pluck(:id).to_a
      RecordLoader.for(Image).load_many(images_ids)
    end
  end
end
