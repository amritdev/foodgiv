# frozen_string_literal: true

module Types
  class NotificationFrequencyEnum < Types::BaseEnum
    value 'INSTANT'
    value 'ONCE_PER_DAY'
    value 'TWICE_PER_DAY'
    value 'NEVER'
  end

  class DeviceInfoInput < Types::BaseInputObject
    argument :device_token, String, required: true
    argument :device_type, DeviceTypeEnum, required: true
  end

  class SaveUserSettingInput < Types::BaseInputObject
    argument :first_name, String, required: false
    argument :last_name, String, required: false
    argument :email, String, required: false
    argument :phone, String, required: false
    argument :password, String, required: false
    argument :location, Types::SavePlaceInput, required: false
    argument :unlink_google, Boolean, required: false
    argument :unlink_facebook, Boolean, required: false
    argument :device_info, DeviceInfoInput, required: false
    argument :notification_allowed, Boolean, required: false
    argument :notification_frequency, NotificationFrequencyEnum, required: false
    argument :notification_distance, Integer, required: false
    argument :google_token, String, required: false
    argument :facebook_token, String, required: false
  end
end
