# frozen_string_literal: true

module Types
  class ReviewEdgeType < GraphQL::Types::Relay::BaseEdge
    node_type(Types::ReviewType)
  end

  class ReviewTypeEnum < Types::BaseEnum
    value 'FOR_GIVER'
    value 'FOR_RECEIVER'
  end

  class ReviewConnectionFilterType < Types::BaseInputObject
    argument :user_id, Integer, required: true
    argument :type, ReviewTypeEnum, required: false
  end

  class ReviewConnectionType < GraphQL::Types::Relay::BaseConnection
    field :total_count, Integer, null: false
    def total_count
      object.nodes.size
    end
    edge_type(Types::ReviewEdgeType)
  end
end
