# frozen_string_literal: true

module Types
  class ProfileType < UserPublicAttributeType
    field :distance, Float, null: true

    def distance
      current_user = context[:current_user]
      object.distance_from(current_user.location)
    end
  end
end
