# frozen_string_literal: true

module Types
  class SaveProfileInput < Types::BaseInputObject
    argument :summary, String, required: true
    argument :favorite_food, String, required: true
    argument :hated_food, String, required: true
    argument :image_id, Integer, required: false
    argument :location, Types::SavePlaceInput, required: false
  end
end
