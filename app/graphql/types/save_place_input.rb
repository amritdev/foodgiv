# frozen_string_literal: true

module Types
  class SavePlaceInput < Types::BaseInputObject
    argument :id, ID, required: false
    argument :place_id, String, required: false
    argument :lat, Float, required: true
    argument :lng, Float, required: true
    argument :formatted_address, String, required: false
  end
end
