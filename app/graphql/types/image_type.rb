# frozen_string_literal: true

module Types
  class CroppedImageType < Types::BaseObject
    field :original_file_name, String, null: false
    field :extension, String, null: false
    field :mime_type, String, null: false
    field :size, Integer, null: false
    field :url, String, null: false
  end
  class BaseImageType < CroppedImageType
    field :id, ID, null: false
  end
  class ImageType < Types::BaseObject
    field :id, ID, null: false
    field :image, BaseImageType, null: true
    field :small, CroppedImageType, null: false
    field :medium, CroppedImageType, null: false
    field :large, CroppedImageType, null: false
  end
end
