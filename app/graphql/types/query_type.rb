# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    include AuthHelper
    include GraphqlHelper

    include CurrentUserQuery
    include LabelsQuery
    include AllergensQuery
    include FoodsConnection
    include FoodQuery
    include FoodRequestSent
    include UserQuery
    include MessagesConnection
    include MyListingConnection
    include RequestsConnection
    include RequestsFoodsConnection
    include MyCompletedRequestsConnection
    include UserReviewsConnection
  end
end
