# frozen_string_literal: true

module Types
  class LocationType < Types::BaseObject
    field :id, ID, null: false
    field :place_id, String, null: true
    field :lat, Float, null: false
    field :lng, Float, null: false
    field :formatted_address, String, null: false
  end
end
