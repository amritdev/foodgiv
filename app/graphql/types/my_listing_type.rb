# frozen_string_literal: true

module Types
  class MyListingType < Types::FoodType
    field :user, Types::UserType, null: false
    field :messages, Types::MessageConnectionType, null: false
    field :requests, Types::RequestConnectionType, null: false

    def user
      RecordLoader.for(User).load(object.user_id)
    end

    def messages
      messages_ids = Message.where(food_id: object.id).pluck(:id).to_a
      RecordLoader.for(Message).load_many(messages_ids)
    end

    def requests
      requests_ids = Request.where(food_id: object.id).pluck(:id).to_a
      RecordLoader.for(Request).load_many(requests_ids)
    end
  end
end
