# frozen_string_literal: true

module Types
  class SaveReviewInput < Types::BaseInputObject
    argument :request_id, Integer, required: true
    argument :collector_exchange_exp, Float, required: false
    argument :food_quality, Float, required: false
    argument :pickup_exp, Float, required: false
    argument :feedback, String, required: false
  end
end
