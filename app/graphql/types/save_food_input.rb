# frozen_string_literal: true

module Types
  class FoodTypeEnum < Types::BaseEnum
    value 'PREPARED'
    value 'UNPREPARED'
  end

  class SaveFoodStatusEnum < Types::BaseEnum
    value 'UNLISTED'
    value 'LISTED'
  end

  class FoodStatusEnum < SaveFoodStatusEnum
    value 'ACCEPTED'
    value 'COMPLETED'
    value 'FAILED'
    value 'DELETED'
  end

  class SaveFoodInput < Types::BaseInputObject
    argument :id, ID, required: false
    argument :title, String, required: true
    argument :description, String, required: true
    argument :food_type, Types::FoodTypeEnum, required: true
    argument :quantity, Integer, required: false
    argument :pickup_time, String, required: false
    argument :exp_date, String, required: false
    argument :labels_ids, [Integer], required: true
    argument :allergens_ids, [Integer], required: true
    argument :images_ids, [Integer], required: true
    argument :location, Types::SavePlaceInput, required: true
    argument :status, Types::SaveFoodStatusEnum, required: false
  end
end
