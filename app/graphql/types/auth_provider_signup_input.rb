# frozen_string_literal: true

module Types
  class SocialAuthProviderEnum < Types::BaseEnum
    value 'GOOGLE'
    value 'FACEBOOK'
  end

  class AuthProviderEnum < SocialAuthProviderEnum
    value 'EMAIL'
    value 'GOOGLE'
    value 'FACEBOOK'
  end

  class AuthProviderSignupInput < Types::BaseInputObject
    graphql_name 'AUTH_PROVIDER_SIGNUP_INPUT'
    argument :provider, AuthProviderEnum, required: true
    argument :first_name, String, required: true
    argument :last_name, String, required: false
    argument :email, String, required: false # facebook might not return email address
    argument :access_token, String, required: false # google users id or facebook users id
    argument :password, String, required: true
  end
end
