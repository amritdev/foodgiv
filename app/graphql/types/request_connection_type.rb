# frozen_string_literal: true

module Types
  class RequestEdgeType < GraphQL::Types::Relay::BaseEdge
    node_type(Types::RequestType)
  end

  class RequestConnectionFilterType < Types::BaseInputObject
    argument :show_archived, Boolean, required: true
    argument :food_id, Integer, required: false
  end

  class RequestConnectionType < GraphQL::Types::Relay::BaseConnection
    field :total_count, Integer, null: false
    def total_count
      object.nodes.size
    end
    edge_type(Types::RequestEdgeType)
  end

  class CompletedRequestFilterEnum < Types::BaseEnum
    value 'GIVE'
    value 'RECEIVE'
    value 'REVIEW_NOT_CREATED'
    value 'REVIEW_CREATED'
    value 'REVIEW_RECEIVED'
  end
end
