# frozen_string_literal: true

module Mutations
  class DeleteAccount < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper

    graphql_name 'delete_account'

    field :success, Boolean, null: true

    def resolve
      auth_check
      begin
        @user = context[:current_user]
        clear_basic_info
        clear_subscription
        clear_social_auth
        delete_foods
        cancel_ongoing_requests
        @user.status = 'DELETED'
        @user.save!(validate: false)
      rescue StandardError => e
        raise e
        return { success: false }
      end
      { success: true }
    end

    def clear_basic_info
      @user.first_name = "user#{@user.created_at.to_time.to_i}#{rand(100..999)}"
      @user.last_name = ''
      @user.phone = ''
      @user.email = ''
      @user.phone_verified = false
      @user.summary = 'This user has been deleted'
      @user.favorite_food = ''
      @user.hated_food = ''
      @user.location = nil
      @user.notification_allowed = false
    end

    def clear_subscription
      if @user.stripe_subscription_id.present?
        begin
          Stripe::Subscription.delete(@user.stripe_subscription_id)
        rescue StandardError => e
          Rails.logger.error(e.message)
        end
        @user.stripe_subscription_id = nil
      end
      if @user.stripe_customer_id.present?
        begin
          Stripe::Customer.delete(@user.stripe_customer_id)
        rescue StandardError => e
          Rails.logger.error(e.message)
        end
      end
      @user.stripe_customer_id = nil
    end

    def clear_social_auth
      @user.google_user_id = nil
      @user.facebook_user_id = nil
    end

    def delete_foods
      Food.where(user_id: @user.id)
          .where(status: 'LISTED')
          .update_all(status: 'UNLISTED')
    end

    def cancel_ongoing_requests
      Request.where(receiver_id: @user.id)
             .where(status: 'NEW')
             .update_all(status: 'DELETED')
    end
  end
end
