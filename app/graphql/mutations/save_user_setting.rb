# frozen_string_literal: true

module Mutations
  class SaveUserSetting < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    include SocialLoginHelper
    graphql_name 'save_user_setting'

    argument :setting, Types::SaveUserSettingInput, required: true
    field :user, Types::UserType, null: false

    def resolve(input)
      auth_check
      @user = context[:current_user]
      @setting = input[:setting]
      save_setting
      { user: @user }
    end

    def save_setting
      check_email_duplication if @setting[:email].present?
      set_base_info
      set_location
      handle_social_link
      set_notification
      @user.save!
    end

    def check_email_duplication
      user_with_same_email = User.where(email: @setting[:email])
                                 .where.not(id: @user.id)
                                 .first
      raise_gql_error code: 'email_duplicated' if user_with_same_email
    end

    def set_base_info
      @user.first_name = @setting[:first_name] if @setting[:first_name].present?
      @user.last_name = @setting[:last_name] if @setting[:last_name].present?
      @user.email = @setting[:email] if @setting[:email].present?
      if @setting[:password].present?
        raise_gql_error code: 'password_too_short' if @setting[:password].length < 8
        @user.password = @setting[:password]
      end

      if @setting[:phone].present?
        @user.phone_verified = false if @user.phone != @setting[:phone]
        @user.phone = @setting[:phone]
      end
    end

    def set_location
      return unless @setting[:location]

      location_attr = @setting[:location]
      location = nil
      location = Location.where(id: location_attr[:id]).first if location_attr[:id].present?
      location ||= Location.new
      location.place_id = location_attr[:place_id]
      location.lat = location_attr[:lat]
      location.lng = location_attr[:lng]
      location.formatted_address = location_attr[:formatted_address]
      location.save!
      @user.location = location
    end

    def handle_social_link
      @user.google_user_id = nil if @setting[:unlink_google]
      @user.facebook_user_id = nil if @setting[:unlink_facebook]
      if @setting[:google_token]
        begin
          google_user = get_google_user(@setting[:google_token])
          google_user_id = google_user['sub']
        rescue StandardError => e
          Rails.logger.info "Link to google user has failed"
          Rails.logger.error e.message
          raise_gql_error code: 'google_connection_failed'
        end
        if User.where(google_user_id: google_user_id).where.not(id: @user.id).exists?
          raise_gql_error code: 'google_user_duplicated'
        else
          @user.google_user_id = google_user_id
        end
      end
      if @setting[:facebook_token]
        begin
          facebook_user = get_facebook_user(@setting[:facebook_token])
          facebook_user_id = facebook_user['id']
          if User.where(facebook_user_id: facebook_user_id).where.not(id: @user.id).exists?
            raise_gql_error code: 'facebook_user_duplicated'
          else
            @user.facebook_user_id = facebook_user_id
          end
        rescue => exception
          Rails.logger.info "Link to facebook user has failed"
          Rails.logger.error exception.message
          raise_gql_error code: 'facebook_connection_failed'
        end
      end
    end

    def set_notification
      @user.device_info = @setting[:device_info] if @setting[:device_info]
      unless @setting[:notification_allowed]
        @user.notification_allowed = false
        return
      end
      @user.notification_allowed = true
      if @setting[:notification_frequency].present?
        @user.notification_frequency = @setting[:notification_frequency]
      end
      if @setting[:notification_distance].present?
        @user.notification_distance = @setting[:notification_distance]
      end
    end
  end
end
