# frozen_string_literal: true

module Mutations
  class VerifyPhone < Mutations::BaseMutation
    include AuthHelper

    graphql_name 'verify_phone'

    argument :code, String, required: true

    field :success, Boolean, null: false
    field :message, String, null: true
    field :code, String, null: true

    def resolve(input)
      auth_check
      @user = context[:current_user]
      if @user.phone_verified
        return { success: false, message: I18n.t('phone_already_verified'), code: 'phone_already_verified' }
      end
      if @user.verification_code.blank?
        return { success: false, message: I18n.t('verification_code_empty'), code: 'verification_code_empty' }
      end
      if @user.verification_code != input[:code]
        return { success: false, message: I18n.t('verification_code_mismatch'), code: 'verification_code_mismatch' }
      end

      @user.phone_verified = true
      @user.save!
      { success: true }
    end
  end
end
