# frozen_string_literal: true

module Mutations
  class ArchiveMessage < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    graphql_name 'archive_message'
    argument :message_id, Integer, required: true
    field :success, Boolean, null: false

    def resolve(input)
      auth_check
      @input = input
      @user = context[:current_user]
      @message = Message.where_belongs_to(@user).where(id: @input[:message_id]).first
      return { success: false } unless @message

      case @message.get_relationship @user
      when 'SENDER'
        @message.sender_archived = true
      when 'RECEIVER'
        @message.receiver_archived = true
      end
      @message.save!
      { success: true }
    end
  end
end
