# frozen_string_literal: true

module Mutations
  class SaveReview < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    graphql_name 'save_review'
    description 'Create or update a review'
    argument :review, Types::SaveReviewInput, required: true
    field :review, Types::ReviewType, null: false

    def resolve(input)
      @input = input[:review]
      auth_check
      check_request
      check_reviewer
      save_review
      save_review_stats_of_other_party
      { review: @review }
    end

    def check_request
      @request = Request.where(id: @input[:request_id]).first
      raise_gql_error code: 'no_such_request' unless @request
      raise_gql_error code: 'request_not_completed' unless @request.status == 'COMPLETED'
    end

    def check_reviewer
      @reviewer = context[:current_user]
      @relationship = @request.get_relationship @reviewer
      raise_gql_error code: 'no_such_request' if @relationship == 'UNKNOWN'
    end

    def save_review
      case @relationship
      when 'GIVER'
        save_giver_review
      when 'RECEIVER'
        save_receiver_review
      end
      @review.request = @request
      @review.food = @request.food
      @review.giver = @request.giver
      @review.receiver = @request.receiver
      @review.reviewer = @reviewer
      @review.save!
    end

    def save_giver_review
      check_rating :collector_exchange_exp
      @review = @request.review_receive
      @review ||= Review.new
      @review.review_type = 'FOR_RECEIVER'
      @review.rating_detail = {
        collector_exchange_exp: @input[:collector_exchange_exp],
        feedback: @input[:feedback]
      }
      @review.rating_overall = @input[:collector_exchange_exp]
    end

    def save_receiver_review
      %i[food_quality pickup_exp].each do |key|
        check_rating(key)
      end
      @review = @request.review_give
      @review ||= Review.new
      @review.review_type = 'FOR_GIVER'
      @review.rating_detail = {
        food_quality: @input[:food_quality],
        pickup_exp: @input[:pickup_exp],
        feedback: @input[:feedback]
      }
      @review.rating_overall = (@input[:food_quality] + @input[:pickup_exp]) / 2
    end

    def save_review_stats_of_other_party
      @reviewee = @request.get_other_party @reviewer
      return unless @reviewee

      case @request.get_relationship @reviewee
      when 'GIVER'
        save_giver_review_stats
      when 'RECEIVER'
        save_receiver_review_stats
      end
    end

    def save_giver_review_stats
      @reviewee.giver_review_count = @reviewee.reviews_received_as_giver.count
      @reviewee.giver_rating = if @reviewee.giver_review_count > 0
                                 @reviewee.reviews_received_as_giver.average(:rating_overall)
                               else
                                 0
                               end
      @reviewee.save!
    end

    def save_receiver_review_stats
      @reviewee.receiver_review_count = @reviewee.reviews_received_as_receiver.count
      @reviewee.receiver_rating = if @reviewee.receiver_review_count > 0
                                    @reviewee.reviews_received_as_receiver.average(:rating_overall)
                                  else
                                    0
                                  end
      @reviewee.save!
    end

    def check_rating(key)
      if @input[key].nil? || @input[key] < 0 || @input[key] > 5
        raise_gql_error code: 'invalid_rating', msg: I18n.t('invalid_rating', rating: key.to_s)
      end
    end
  end
end
