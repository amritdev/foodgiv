# frozen_string_literal: true

module Mutations
  class SendRequest < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    include AppearanceHelper
    include RpushHelper
    graphql_name 'send_request'
    description 'Send a request for a food'
    argument :food_id, Integer, required: true
    argument :message, String, required: false

    field :request, Types::RequestType, null: false

    def resolve(input)
      auth_check
      @input = input
      check_action
      save_request
      send_message
      notify_to_giver
      { request: @request }
    end

    def check_action
      check_receiver
      check_food
      check_giver
      raise_gql_error code: 'request_already_sent' unless first_time?
    end

    def check_receiver
      @receiver = context[:current_user]
      raise_gql_error code: 'user_not_paid' unless @receiver.subscribed?
    end

    def check_food
      @food = Food.where(id: @input[:food_id]).first
      raise_gql_error code: 'no_such_food' unless @food
      raise_gql_error code: 'food_not_listed' if @food.status != 'LISTED'
      raise_gql_error code: 'food_expired' if @food.expired?
    end

    def check_giver
      @giver = @food.user
      raise_gql_error code: 'giver_not_found' unless @giver || @giver.active?
      raise_gql_error code: 'cannot_send_request_to_own_food' if @giver.id == @receiver.id
    end

    def first_time?
      Request.where(food_id: @food.id, receiver_id: @receiver.id).count == 0
    end

    def save_request
      @request = Request.new
      @request.giver = @giver
      @request.receiver = @receiver
      @request.food = @food
      @request.message = @input[:message].presence || "Hi, I'm interested in your food #{@food.title}"
      @request.read = false
      @request.status = 'NEW'
      @request.save!
    end

    def send_message
      @message = Message.create(
        request_id: @request.id,
        food_id: @food.id,
        sender_id: @receiver.id,
        receiver_id: @giver.id,
        content: @request.message,
        content_type: 'TEXT'
      )
      MessageBroadcastJob.perform_later(message: @message, sender: @receiver)
    end

    def notify_to_giver
      return if online? user: @giver

      push_request_event user: @giver, request: @request, event: 'new'
    end
  end
end
