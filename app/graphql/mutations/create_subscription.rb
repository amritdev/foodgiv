# frozen_string_literal: true

require 'stripe'

module Mutations
  class CreateSubscription < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    graphql_name 'create_subscription'
    argument :payment_id, String, required: true

    field :user, Types::UserType, null: false

    def resolve(input)
      auth_check
      @input = input
      @user = context[:current_user]
      raise_gql_error code: 'user_already_paid' if @user.stripe_subscription_id.present?
      retrieve_payment_method
      get_stripe_customer_from_user
      attach_payment_method_to_customer
      create_subscription
      @user.stripe_customer_id = @customer[:id]
      @user.stripe_subscription_id = @subscription[:id]
      @user.save!
      { user: @user }
    end

    def retrieve_payment_method
      @payment = Stripe::PaymentMethod.retrieve @input[:payment_id]
    end

    def get_stripe_customer_from_user
      @customer = nil
      if @user.stripe_customer_id
        begin
          @customer = Stripe.customer.retrieve @user.stripe_customer_id
        rescue StandardError
          @customer = nil
        end
      end
      @customer ||= Stripe::Customer.create({
                                              email: @user.email,
                                              name: "#{@user.first_name} #{@user.last_name}",
                                              phone: @user.phone,
                                              payment_method: @payment[:id]
                                            })
    end

    def attach_payment_method_to_customer
      Stripe::PaymentMethod.attach(@payment[:id], {
                                     customer: @customer[:id]
                                   })
    end

    def create_subscription
      @subscription = Stripe::Subscription.create({
                                                    customer: @customer[:id],
                                                    default_payment_method: @payment[:id],
                                                    items: [
                                                      {
                                                        price: Rails.configuration.stripe.price_id
                                                      }
                                                    ]
                                                  })
    end
  end
end
