# frozen_string_literal: true

module Mutations
  class NotificationPriorityEnum < Types::BaseEnum
    value 'normal'
    value 'high'
  end

  class PushNotificationTest < Mutations::BaseMutation
    graphql_name 'push_notification_test'
    argument :to, Integer, required: true
    argument :data, String, required: true, description: "custom key-value pairs of the message's payload"
    argument :priority, NotificationPriorityEnum, required: false
    argument :content_available, Boolean, required: false
    argument :notification, String, required: false, description: 'predefined, user-visible key-value pairs of the notification payload'

    field :success, Boolean, null: false

    def resolve(input)
      unless Rails.env.development? || Rails.env.test?
        raise_gql_error code: 'invalid_request', msg: 'This mutation is not supported in production environment'
      end
      user = User.find input[:to]
      data = JSON.parse(input[:data])
      notification = JSON.parse(input[:notification])
      PushNotification.create_fcm_notification(user: user, data: data, notification: notification, collapse_key: 'Test notification')
      Rpush.push
      { success: true }
    end
  end
end
