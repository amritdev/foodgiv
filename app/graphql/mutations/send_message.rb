# frozen_string_literal: true

module Mutations
  class MessageContentTypeEnum < Types::BaseEnum
    value 'TEXT'
    value 'IMAGE'
  end

  class SendMessage < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    graphql_name 'send_message'
    description 'Send a message on a request. Food receiver can send messages only after a giver sends a message'
    argument :request_id, Integer, required: true
    argument :content, String, required: true
    argument :content_type, MessageContentTypeEnum, required: true

    field :message, Types::MessageType, null: true

    def resolve(input)
      auth_check
      @input = input
      check_action
      save_message
      notify_to_users
      { message: @message }
    end

    def check_action
      check_request
      check_food
      check_sender
      check_receiver
      check_message
    end

    def check_request
      @request = Request.where(id: @input[:request_id]).first
      raise_gql_error code: 'no_such_request' unless @request
    end

    def check_sender
      @sender = context[:current_user]
      unless @request.related? @sender
        raise_gql_error code: 'cannot_send_message_to_unrelated_request'
      end
      if @request.receiver? @sender
        if @request.accepted_at.nil? && !@request.giver_sent_message?
          raise_gql_error code: 'cannot_send_message_before_giver'
        end
      end
    end

    def check_food
      @food = @request.food
      raise_gql_error code: 'no_such_food' unless @food
    end

    def check_receiver
      @receiver = @request.get_other_party @sender
      raise_gql_error code: 'receiver_not_available' unless @receiver&.active?
    end

    def check_message; end

    def save_message
      @message = Message.new
      @message.sender = @sender
      @message.receiver = @receiver
      @message.request = @request
      @message.food = @food
      @message.content = @input[:content]
      @message.content_type = @input[:content_type]
      @message.save!
    end

    def notify_to_users
      BroadcastJob.perform_later room: "requests:#{@request.id}", data: @message, event: 'MessageUpdated'
      [@sender, @receiver].each do |user|
        BroadcastJob.perform_later room: "users:#{user.id}", event: 'InboxUpdated'
      end
    end
  end
end
