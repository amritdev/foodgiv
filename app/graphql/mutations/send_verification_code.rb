# frozen_string_literal: true

module Mutations
  class SendVerificationCode < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    include TwilioHelper
    THROTTLE_SEC = 5

    graphql_name 'send_verification_code'

    argument :phone, String, required: true
    field :success, Boolean, null: false

    def resolve(input)
      auth_check
      @user = context[:current_user]
      check_throttle
      @user.verification_code = generate_pin
      @user.phone = input[:phone]
      send_code_to_user
      mark_last_request
      @user.save!
      { success: true }
    end

    def check_throttle
      if Rails.cache.read(last_request_cache_key).present?
        raise_gql_error code: 'otp_too_many_requests', msg: I18n.t('otp_too_many_requests', time: THROTTLE_SEC.to_s)
      end
    end

    def send_code_to_user
      message = "Your verification code for foodgiv app is #{@user.verification_code}"
      begin
        send_sms(@user, message)
      rescue Twilio::REST::RestError => e
        Rails.logger.error e.message
        raise_gql_error code: 'sms_fail'
      end
    end

    def mark_last_request
      Rails.cache.write(last_request_cache_key, DateTime.now, expires_in: THROTTLE_SEC.seconds)
    end

    def generate_pin
      rand(0o10000..999_999).to_s.rjust(6, '0')
    end

    def last_request_cache_key
      "users:#{@user.id}:phone-verify:last-request-time"
    end
  end
end
