# frozen_string_literal: true

module Mutations
  class SendOtp < Mutations::BaseMutation
    include GraphqlHelper
    include TwilioHelper
    EXPIRE_HOUR = 3
    graphql_name 'send_otp'
    description 'Send OTP(one-time password)'
    argument :email, String, required: true

    field :success, Boolean, null: false

    def resolve(input)
      @input = input
      email = @input[:email]
      raise_gql_error code: 'email_empty' if email.blank?
      @user = User.where(email: email).first
      raise_gql_error code: 'no_such_user' unless @user
      @otp = get_user_otp
      Rails.logger.debug "otp for user id: #{@user.id} is #{@otp}"
      begin
        send_otp_to_user
      rescue StandardError => e
        Rails.logger.error 'Cannot send OTP to this user'
        Rails.logger.info @user.inspect
        Rails.logger.error(e.message)
        return { success: false }
      end
      { success: true }
    end

    def send_otp_to_user
      if @user.phone && @user.phone_verified
        begin
          send_otp_via_sms
        rescue StandardError => e
          Rails.logger.warn 'Cannot send OTP via sms to this user'
          Rails.logger.info @user.inspect
          Rails.logger.warn(e.message)
        end
      end
      send_otp_via_email
    end

    def send_otp_via_sms
      message = "Your OTP code for foodgiv app is #{@otp}. This code will be expired in #{EXPIRE_HOUR} hours"
      send_sms(@user, message)
    end

    def send_otp_via_email
      UserMailer.with(user: @user, otp: @otp).otp_mail.deliver_later
    end

    def get_user_otp
      Rails.cache.fetch("users:#{@user.id}:otp", expires_in: EXPIRE_HOUR.hours) do
        rand(0o1000000..99_999_999).to_s.rjust(8, '0')
      end
    end
  end
end
