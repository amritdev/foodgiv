# frozen_string_literal: true

module Mutations
  class DeclineRequest < Mutations::BaseMutation
    include AuthHelper
    include GraphqlHelper
    include AppearanceHelper
    include RpushHelper
    graphql_name 'decline_request'
    description 'Giver declines a request'
    argument :request_id, Integer, required: true

    field :request, Types::RequestType, null: false

    def resolve(input)
      @input = input
      auth_check
      check_request
      save_request
      notify_to_receiver
      { request: @request }
    end

    def check_request
      @user = context[:current_user]
      @request = Request.where(id: @input[:request_id], giver_id: @user.id).first
      raise_gql_error code: 'no_such_request' unless @request
      unless @request.status == 'NEW'
        raise_gql_error code: "request_already_#{@request.status.downcase}", msg: I18n.t('graphql.errors.request_invalid_action', action: 'decline', status: @request.status.downcase)
      end
    end

    def save_request
      @request.declined_at = DateTime.now
      @request.status = 'DECLINED'
      @request.save!
    end

    def notify_to_receiver
      return if online? user: @receiver

      push_request_event user: @receiver, request: @request, event: 'declined'
    end
  end
end
