# frozen_string_literal: true

class Allergen < ApplicationRecord
  after_save :clear_cache
  after_destroy :clear_cache
  has_and_belongs_to_many :foods, join_table: :foods_allergens

  validates :name, presence: true, length: { minimum: 3, maximum: 191 }

  def clear_cache
    Rails.cache.delete('allergens')
  end
end
