# frozen_string_literal: true

class Message < ApplicationRecord
  belongs_to :sender, class_name: 'User'
  belongs_to :receiver, class_name: 'User'
  belongs_to :food, optional: true
  belongs_to :request, optional: true

  scope :where_belongs_to, ->(user) { where(sender_id: user.id).or(where(receiver_id: user.id)) }
  scope :archived, ->(user) { where(sender_id: user.id, sender_archived: true).or(where(receiver_id: user.id, receiver_archived: true)) }
  scope :unarchived, ->(user) { where(sender_id: user.id, sender_archived: false).or(where(receiver_id: user.id, receiver_archived: false)) }
  scope :unread, ->(user) { where(receiver_id: user.id).unarchived(user).where(read_at: nil) }

  def get_relationship(user)
    return 'UNKNOWN' unless user
    return 'SENDER' if self[:sender_id] == user.id
    return 'RECEIVER' if self[:receiver_id] == user.id

    'UNKNOWN'
  end

  def archived?(user)
    case get_relationship user
    when 'SENDER'
      sender_archived
    when 'RECEIVER'
      receiver_archived
    else
      false
    end
  end

  def read?
    !read_at.nil?
  end
end
