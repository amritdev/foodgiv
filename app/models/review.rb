# frozen_string_literal: true

class Review < ApplicationRecord
  belongs_to :food
  belongs_to :request
  belongs_to :giver, class_name: 'User'
  belongs_to :receiver, class_name: 'User'
  belongs_to :reviewer, class_name: 'User'

  validates :rating_detail, presence: true
  validates :rating_overall, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 5 }

  scope :where_created_by, ->(user) { where(reviewer_id: user.id) }
  scope :where_belongs_to, ->(user) { where(giver_id: user.id).or(where(receiver_id: user.id)) }
  scope :where_received_by, ->(user) { where_belongs_to(user).where.not(reviewer_id: user.id) }

  after_save :notify_to_channels

  def rating_detail=(rating_detail)
    self[:rating_detail] = if rating_detail.is_a?(String)
                             rating_detail
                           else
                             JSON.generate rating_detail
                           end
  end

  def rating_detail
    hash = begin
             JSON.parse self[:rating_detail]
           rescue StandardError
             {}
           end
    hash
  end

  def food_quality
    rating_detail['food_quality']
  end

  def pickup_exp
    rating_detail['pickup_exp']
  end

  def collector_exchange_exp
    rating_detail['collector_exchange_exp']
  end

  def feedback
    rating_detail['feedback']
  end

  private

  def notify_to_channels
    # Request Channel
    BroadcastJob.perform_later room: "requests:#{request.id}", data: self, event: 'ReviewUpdated'
    # Appearance Channel
    [giver, receiver].each do |user|
      BroadcastJob.perform_later room: "users:#{user.id}", event: 'InboxUpdated'
    end
  end
end
