# frozen_string_literal: true

class Image < ApplicationRecord
  include ImageUploader::Attachment(:image)
  has_and_belongs_to_many :foods

  def small
    image(:small).nil? ? image : image(:small)
  end

  def medium
    image(:medium).nil? ? image : image(:medium)
  end

  def large
    image(:large).nil? ? image : image(:large)
  end
end
