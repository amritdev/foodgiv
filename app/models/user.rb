# frozen_string_literal: true

class User < ApplicationRecord
  # default_scope { where(status: 'ACTIVE') }
  scope :disabled, -> { unscope(where: :status).where(status: 'DISABLED') }
  scope :deleted, -> { unscope(where: :status).where(status: 'DELETED') }
  scope :where_email_icase, ->(email) { where('LOWER(email) = ?', email.downcase) }

  belongs_to :location, optional: true
  belongs_to :profile_image, class_name: 'Image', foreign_key: 'image_id', optional: true

  validates :first_name, presence: true, length: { maximum: 50 }
  validates :last_name, length: { maximum: 50 }
  validates :email, length: { maximum: 191 }, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :phone, length: { maximum: 20 }
  validates :password, length: { minimum: 8 }, on: :create, unless: :social_linked?
  validates :favorite_food, length: { maximum: 255 }
  validates :hated_food, length: { maximum: 255 }
  validates :summary, length: { maximum: 511 }

  has_secure_password

  def subscribed?
    stripe_customer_id.present? && stripe_subscription_id.present?
  end

  def active?
    status == 'ACTIVE'
  end

  def reviews_received_as_giver
    Review.where(giver_id: id).where.not(reviewer_id: id)
  end

  def reviews_received_as_receiver
    Review.where(receiver_id: id).where.not(reviewer_id: id)
  end

  def distance_from(mappable, units = :miles)
    if location.nil? || mappable.nil?
      nil
    else
      Rails.cache.fetch("distance:#{location.id}-#{mappable.id}", expires_in: 7.days) do
        location.distance_from(mappable, units: units)
      end
    end
  end

  def social_linked?
    google_user_id.present? || facebook_user_id.present?
  end
end
