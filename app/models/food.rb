# frozen_string_literal: true

class Food < ApplicationRecord
  default_scope { where.not(status: 'DELETED') }
  scope :listed, -> { where(status: 'LISTED').where('exp_date > ?', DateTime.now.strftime('%Y-%m-%d %H:%M:%S')) }
  scope :expired, -> { where('exp_date <= ?', DateTime.now.strftime('%Y-%m-%d %H:%M:%S')) }
  scope :deleted, -> { unscope(where: :status).where(status: 'DELETED') }
  scope :where_has_requests, -> { includes(:requests).where.not(requests: { id: nil }) }
  scope :where_has_requests_belongs_to_user, ->(user) { joins(:requests).merge(Request.where_belongs_to(user)) }
  scope :lately_listed_in, ->(time) { listed.where('foods.created_at >= ?', (DateTime.now - time).strftime('%Y-%m-%d %H:%M:%S')) }
  belongs_to :user
  belongs_to :location
  has_and_belongs_to_many :labels
  has_and_belongs_to_many :allergens, join_table: :foods_allergens
  has_and_belongs_to_many :images
  has_many :requests
  has_many :messages

  acts_as_mappable through: :location

  validates :title, presence: true, length: { maximum: 50 }
  validates :description, length: { minimum: 9, maximum: 300 }
  validates :status, presence: true

  def expired?
    exp_date.present? && (exp_date <= DateTime.now)
  end

  def request_count
    requests.count
  end

  def message_count
    messages.count
  end

  def last_action_at
    if requests.empty?
      created_at
    else
      requests.sort_by(&:last_action_at).reverse.first.last_action_at
    end
  end
end
