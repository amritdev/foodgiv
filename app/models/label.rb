# frozen_string_literal: true

class Label < ApplicationRecord
  after_save :clear_cache
  after_destroy :clear_cache
  validates :name, presence: true, length: { minimum: 3, maximum: 191 }

  has_and_belongs_to_many :foods

  def clear_cache
    Rails.cache.delete('labels')
  end
end
