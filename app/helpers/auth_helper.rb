# frozen_string_literal: true

module AuthHelper
  def get_current_user(headers)
    auth_header = http_auth_header(headers)
    token = auth_header.gsub(/^Bearer /, '')
    user_id = get_decoded_auth_token(token)
    begin
      user_id ? User.find(user_id) : nil
    rescue StandardError
      nil
    end
  end

  def get_current_user_from(headers: nil, token: nil, request: nil)
    token = if headers
              auth_header = http_auth_header(headers)
              auth_header.gsub(/^Bearer /, '')
            elsif request
              request.params[:token]
            else
              token
            end
    user_id = get_decoded_auth_token(token)
    begin
      user_id ? User.find(user_id) : nil
    rescue StandardError
      nil
    end
  end

  def http_auth_header(headers)
    if headers['Authorization'].present?
      headers['Authorization'].split(' ').last
    else
      ''
    end
  end

  def get_decoded_auth_token(token)
    data = JsonWebToken.decode(token)
    begin
      data[:data]
    rescue StandardError
      nil
    end
  end

  def save_login_info(user)
    request = context[:request]
    return user unless request

    user.current_sign_in_at = DateTime.now
    user.last_sign_in_ip = user.current_sign_in_ip
    user.current_sign_in_ip = request.remote_ip
    user.save
  end

  def raise_authentication_exception
    raise GraphQL::ExecutionError.new(I18n.t('graphql.errors.authentication_failed'), extensions: { code: 'AUTHENTICATION_FAILED' })
  end

  def auth_check
    raise_authentication_exception if context[:current_user].blank?
  end
end
