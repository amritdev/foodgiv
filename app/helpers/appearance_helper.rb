# frozen_string_literal: true

module AppearanceHelper
  def register_appearance(user:, room: '')
    users = get_appearance_list(room)
    users << user.id unless users.include? user.id
    set_appearance_list users, room
  end

  def unregister_appearance(user:, room: '')
    users = get_appearance_list(room)
    users.delete(user.id)
    set_appearance_list users, room
  end

  def online?(user:, room: '')
    return false unless user.present? && user.id.present?

    get_appearance_list(room).include? user.id
  end

  def get_appearance_list(room)
    room = absolute_path(room)
    Rails.cache.read(room) || []
  end

  def set_appearance_list(users, room)
    room = absolute_path(room)
    if users.empty?
      Rails.cache.delete room
    else
      Rails.cache.write room, users, expires_in: 100.years
    end
  end

  def remove_appearance_list(room)
    Rails.cache.delete room
  end

  def absolute_path(relative)
    room = relative.blank? ? 'appearance:users' : "appearance:#{relative}"
  end
end
