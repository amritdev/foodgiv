# frozen_string_literal: true

module GraphqlHelper
  def raise_gql_error(code:, msg: '')
    msg = I18n.t "graphql.errors.#{code}" if msg.blank?
    raise GraphQL::ExecutionError.new(msg, extensions: { code: code })
  end
end
