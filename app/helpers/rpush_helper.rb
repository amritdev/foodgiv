# frozen_string_literal: true

module RpushHelper
  def push_request_event(request:, event:, user:)
    event = "request_#{event}"
    data = {
      request_id: request.id,
      event: event
    }
    notification = {
      title: I18n.t("push.#{event}.title"),
      body: I18n.t("push.#{event}.body", giver: request.giver.first_name, receiver: request.receiver.first_name, food: request.food.title)
    }
    if Rails.env.test?
      puts 'PushNotification data would look like as the following:'
      puts({
        data: data,
        collapse_key: event,
        notification: notification
      }.inspect)
      return
    end
    PushNotification.create_fcm_notification(
      user: user,
      data: data,
      collapse_key: event,
      notification: notification
    )
  end

  def notification_subscribed_users(frequency:)
    if frequency == 'INSTANT'
      Rails.cache.fetch("notification:users:#{frequency}", expires_in: 1.hour) do
        uncached_notification_subscribed_users frequency
      end
    else
      uncached_notification_subscribed_users frequency
    end
  end

  def uncached_notification_subscribed_users(frequency)
    User.where(notification_allowed: true)
        .where(status: 'ACTIVE')
        .where(notification_frequency: frequency)
        .where(phone_verified: true)
        .where.not(stripe_customer_id: [nil, ''])
        .where.not(device_info: [nil, ''])
        .where.not(location_id: nil)
        .all
  end
end
