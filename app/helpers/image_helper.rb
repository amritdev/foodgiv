# frozen_string_literal: true

module ImageHelper
  def new_params
    params.permit(:image)
  end

  def multiple_upload_params
    params.permit(images: {})
  end

  def to_hash(image)
    {
      id: image.id,
      size: image.image.size,
      height: image.image.height,
      width: image.image.width,
      image: image.image.url,
      small: image.image(:small).url,
      medium: image.image(:medium).url,
      large: image.image(:large).url
    }
  end
end
