# frozen_string_literal: true

module TwilioHelper
  def send_sms(user, message)
    return send_mockup_sms(user, message) if Rails.env.test?

    twilio = Twilio::REST::Client.new
    twilio.messages.create(
      from: Rails.configuration.twilio.number,
      to: user.phone,
      body: message
    )
  end

  def send_mockup_sms(user, message)
    puts "SMS Mockup Service says that message \"#{message}\" was sent to '#{user.phone}'"
  end
end
