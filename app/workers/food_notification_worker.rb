# frozen_string_literal: true

class FoodNotificationWorker
  include Sidekiq::Worker

  def perform(frequency)
    FoodNotificationJob.perform_now frequency: frequency
  end
end
