# frozen_string_literal: true

class ExpireCompletedPushNotificationsWorker
  include Sidekiq::Worker

  def perform(*_args)
    ExpireCompletedPushNotificationsJob.perform_now
  end
end
