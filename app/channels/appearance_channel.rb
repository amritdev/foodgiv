# frozen_string_literal: true

class AppearanceChannel < ApplicationCable::Channel
  include AppearanceHelper
  def subscribed
    register_appearance(user: current_user)
    Rails.logger.debug "#{DateTime.now} AppearanceChannel subscription: #{room_name}"
    stream_from room_name
  end

  def disconnect
    Rails.logger.debug "#{DateTime.now} AppearanceChannel disconnect"
    unsubscribed
  end

  def unsubscribed
    Rails.logger.debug "#{DateTime.now} AppearanceChannel unsubscription: #{room_name}"
    unregister_appearance(user: current_user)
  end

  def room_name
    "users:#{current_user.id}"
  end
end
