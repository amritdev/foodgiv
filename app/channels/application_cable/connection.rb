# frozen_string_literal: true

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    include AuthHelper
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
    end

    private

    def find_verified_user
      if user = get_current_user_from(request: request)
        user
      else
        reject_unauthorized_connection
      end
    end
  end
end
