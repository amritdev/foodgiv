# frozen_string_literal: true

class RequestChannel < ApplicationCable::Channel
  include AppearanceHelper
  def subscribed
    request_id = params[:request_id]
    @request = Request.where_belongs_to(current_user).where(id: request_id).first
    reject_unauthorized_connection if @request.nil?
    register_appearance(user: current_user, room: room_name)
    read_all_messages
    Rails.logger.debug "#{DateTime.now} RequestChannel subscription: #{room_name}"
    stream_from room_name
  end

  def save_message(data)
    begin
      @receiver = @request.get_other_party(current_user)
      unless @receiver.present? && @receiver.active?
        BroadcastJob.perform_later room: room_name, data: I18n.t('graphql.errors.receiver_not_available'), event: 'Error'
        return
      end
      if @request.receiver? current_user
        if @request.accepted_at.nil? && !@request.giver_sent_message?
          BroadcastJob.perform_later room: room_name, data: I18n.t('graphql.errors.cannot_send_message_before_giver'), event: 'Error'
          return
        end
      end
      message = Message.create!(
        request_id: @request.id,
        food_id: @request.food.id,
        sender_id: current_user.id,
        receiver_id: @request.get_other_party(current_user).id,
        content: data['content'],
        content_type: data['content_type'] || 'TEXT',
        read_at: receiver_present? ? DateTime.now : nil
      )
    rescue StandardError => e
      Rails.logger.error e.message
      message = nil
    end
    BroadcastJob.perform_later room: room_name, data: message, event: 'MessageUpdated'
    [current_user, @receiver].each do |user|
      BroadcastJob.perform_later room: "users:#{user.id}", event: 'InboxUpdated'
    end
  end

  def read_all_messages
    @request.messages.unread(current_user).update_all(read_at: DateTime.now)
  end

  def unsubscribed
    Rails.logger.debug "#{DateTime.now} RequestChannel unsubscription: #{room_name}"
    unregister_appearance(user: current_user, room: room_name)
  end

  def room_name
    "requests:#{@request.id}"
  end

  def receiver_present?
    online?(user: @receiver, room: room_name)
  end
end
