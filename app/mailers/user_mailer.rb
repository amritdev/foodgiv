# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def otp_mail
    @user = params[:user]
    @otp = params[:otp]
    mail(to: @user.email, subject: 'Foodgiv OTP code arrived')
  end
end
