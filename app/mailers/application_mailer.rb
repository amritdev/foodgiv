# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'support@foodgiv.com'
  layout 'mailer'
end
