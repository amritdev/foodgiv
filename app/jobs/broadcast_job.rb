# frozen_string_literal: true

class BroadcastJob < ApplicationJob
  queue_as :default

  def perform(room:, data: nil, event: '')
    ActionCable.server.broadcast(room, { data: data, event: event })
  end
end
