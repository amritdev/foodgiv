# frozen_string_literal: true

class MessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message: nil)
    @message = message
    if @message.nil?
      nil
    else
      @receiver = message.receiver
      ActionCable.server.broadcast(
        "chat_#{@receiver.id}",
        message: @message
      )
    end
  end
end
