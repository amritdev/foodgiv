# frozen_string_literal: true

class PushNotificationJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    Rpush.push
  end
end
