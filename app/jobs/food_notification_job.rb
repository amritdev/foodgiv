# frozen_string_literal: true

class FoodNotificationJob < ApplicationJob
  include RpushHelper
  queue_as :default

  def perform(frequency:)
    @frequency = frequency
    Rails.logger.info("Food notification job for frequency: #{@frequency}")
    return if food_time_threshold.nil?

    foods = Food.lately_listed_in(food_time_threshold)
                .order(created_at: :desc, id: :desc)
    Rails.logger.info("Recently added listing count: #{foods.count}")
    return if foods.count < 1

    users = notification_subscribed_users frequency: @frequency
    Rails.logger.info("Users subscribed to notification: #{users.count}")
    notification_count = 0
    users.each do |user|
      new_foods = foods.joins(:location)
                       .where.not(user_id: user.id)
                       .within(user.notification_distance, origin: user.location)
      count = new_foods.count
      next unless count > 0

      food = new_foods.first
      notification = {
        title: I18n.t('push.food_new.title'),
        body: if count > 1
                I18n.t('push.food_new.body.multiple', food: food.title, count: count - 1)
              else
                I18n.t('push.food_new.body.single', food: food.title)
              end
      }

      if Rails.env.test?
        puts 'PushNotification notification looks like follows:'
        puts notification.inspect
        notification_count += 1
        next
      end
      PushNotification.create_fcm_notification(
        user: user,
        collapse_key: 'food_new',
        notification: notification
      )
      notification_count += 1
    end
    Rails.logger.info("Notified to #{notification_count} users")
    Rails.logger.info("Food notification job for frequency #{@frequency} ended successfully.")
  end

  def food_time_threshold
    case @frequency
    when 'INSTANT'
      5.minutes
    when 'TWICE_PER_DAY'
      12.hours
    when 'ONCE_PER_DAY'
      24.hours
    end
  end
end
