# frozen_string_literal: true

labels = [
  'Vegan',
  'Vegetarian',
  'Fresh Produce',
  'Packaged Food',
  'Raw',
  'Organic',
  'Gluten-Free',
  'Whole Grain',
  'Oil-Free',
  'Koshwer'
]

count = 0

labels.each do |name|
  unless Label.find_by(name: name)
    Label.create!(name: name)
    count += 1
  end
end

puts "Seeded #{count} labels."
