# frozen_string_literal: true

require 'down'

class Hash
  def except_nested(key)
    r = Marshal.load(Marshal.dump(self)) # deep copy the hashtable
    r.except_nested!(key)
  end

  def except_nested!(key)
    except!(key)
    each do |_, v| # essentially dfs traversal calling except!
      v.except_nested!(key) if v.is_a?(Hash)
    end
  end
end

user_attributes = [
  {
    first_name: 'brave',
    last_name: 'master',
    email: 'bravemaster619@hotmail.com',
    phone: '1234567890',
    phone_verified: true,
    password: '11111111',
    profile_image: 'https://en.gravatar.com/userimage/183838592/43066144adcecf74471c53e484ea29ac.png',
    summary: '5+ years of experience in web development. Love junk food but stay fit.',
    favorite_food: 'barbeque,coke,chicken,',
    hated_food: 'squid,crab,',
    notification_frequency: 'INSTANT',
    notification_distance: 25,
    status: 'ACTIVE',
    location: {
      place_id: 'ChIJB8b9vcQKSF4RiJpv0Pf7UPc',
      lat: 43.9108,
      lng: -126.5622,
      formatted_address: 'Longtan District, Jilin City'
    },
    created_at: DateTime.now
  }
]

count = 0

user_attributes.each do |attribute|
  user = User.where(email: attribute[:email]).first
  unless user
    user = User.create(attribute.except_nested(:location).except_nested(:profile_image))
    count += 1
  end
  if attribute[:location].present?
    location = Location.where(place_id: attribute[:location][:place_id]).first
    location ||= Location.create(attribute[:location])
    user[:location_id] = location.id
    user.save
  end
  next if attribute[:profile_image].blank?

  pic = Image.new
  pic.image = Down.open attribute[:profile_image]
  pic.image_derivatives!
  user.profile_image = pic
  user.save
end

puts "Seeded #{count} users."
