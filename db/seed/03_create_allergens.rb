# frozen_string_literal: true

allergens = [
  'Milk',
  'Shellfish',
  'Wheat',
  'Egg',
  'Fish',
  'Peanut',
  'All Nuts'
]

count = 0

allergens.each do |name|
  unless Allergen.find_by(name: name)
    Allergen.create!(name: name)
    count += 1
  end
end

puts "Seeded #{count} allergens."
