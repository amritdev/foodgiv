# frozen_string_literal: true

class CreateFoodsImages < ActiveRecord::Migration[5.2]
  def change
    create_table :foods_images do |t|
      t.belongs_to :food
      t.belongs_to :image
    end
  end
end
