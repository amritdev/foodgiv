# frozen_string_literal: true

class CreateRequest < ActiveRecord::Migration[5.2]
  def change
    create_table :requests do |t|
      t.bigint :giver_id, null: false
      t.bigint :receiver_id, null: false
      t.belongs_to :food
      t.text :message
      t.datetime :accepted_at
      t.datetime :declined_at
      t.datetime :cancelled_at
      t.datetime :completed_at
      t.boolean :read, default: false
      t.boolean :giver_archived, default: false
      t.boolean :receiver_archived, default: false
      t.string :status
      t.timestamps
    end
    add_foreign_key :requests, :users, column: :giver_id
    add_foreign_key :requests, :users, column: :receiver_id
  end
end
