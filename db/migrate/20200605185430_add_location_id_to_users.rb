# frozen_string_literal: true

class AddLocationIdToUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :location, foreign_key: true, after: :hated_food
  end
end
