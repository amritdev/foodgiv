# frozen_string_literal: true

class CreateReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.belongs_to :request, null: false
      t.belongs_to :food, null: false
      t.bigint :giver_id, null: false
      t.bigint :receiver_id, null: false
      t.bigint :reviewer_id, null: false
      t.text :rating_detail, null: false
      t.float :rating_overall, null: false
      t.string :review_type, null: false
      t.timestamps
    end
    add_foreign_key :reviews, :users, column: :giver_id
    add_foreign_key :reviews, :users, column: :receiver_id
    add_foreign_key :reviews, :users, column: :reviewer_id
  end
end
