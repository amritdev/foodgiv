# frozen_string_literal: true

class RemoveProfileImageFromUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :profile_image
  end
end
