# frozen_string_literal: true

class CreateFoodsLabels < ActiveRecord::Migration[5.2]
  def change
    create_table :foods_labels do |t|
      t.belongs_to :food
      t.belongs_to :label
    end
  end
end
