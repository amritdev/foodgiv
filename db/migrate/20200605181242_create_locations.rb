# frozen_string_literal: true

class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.string :place_id, null: true
      t.float :lat, null: false
      t.float :lng, null: false
      t.string :formatted_address, null: false

      t.timestamps
    end
  end
end
