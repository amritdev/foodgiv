# frozen_string_literal: true

class CreateMessage < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.bigint :sender_id, null: false
      t.bigint :receiver_id, null: false
      t.belongs_to :request
      t.belongs_to :food
      t.text :content
      t.string :content_type, default: 'TEXT'
      t.datetime :read_at
      t.boolean :sender_archived, default: false
      t.boolean :receiver_archived, default: false

      t.timestamps
    end
    add_foreign_key :messages, :users, column: :sender_id
    add_foreign_key :messages, :users, column: :receiver_id
  end
end
