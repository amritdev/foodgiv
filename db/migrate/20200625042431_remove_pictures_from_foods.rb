# frozen_string_literal: true

class RemovePicturesFromFoods < ActiveRecord::Migration[5.2]
  def change
    remove_column :foods, :pictures
  end
end
