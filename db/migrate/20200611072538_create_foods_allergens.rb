# frozen_string_literal: true

class CreateFoodsAllergens < ActiveRecord::Migration[5.2]
  def change
    create_table :foods_allergens do |t|
      t.belongs_to :food
      t.belongs_to :allergen
    end
  end
end
