# frozen_string_literal: true

class CreateFoods < ActiveRecord::Migration[5.2]
  def change
    create_table :foods do |t|
      t.belongs_to :user
      t.string :title, null: false
      t.text :description
      t.string :food_type, null: false
      t.integer :quantity, default: 1
      t.string :pickup_time
      t.datetime :exp_date
      t.belongs_to :location
      t.text :pictures
      t.string :status

      t.timestamps
    end
  end
end
