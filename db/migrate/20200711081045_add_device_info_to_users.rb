# frozen_string_literal: true

class AddDeviceInfoToUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :device_token
    add_column :users, :device_info, :json, null: true, after: :receiver_rating
  end
end
