# frozen_string_literal: true

class ChangeLatLongFromLocations < ActiveRecord::Migration[5.2]
  def change
    change_column :locations, :lat, :decimal, precision: 18, scale: 12
    change_column :locations, :lng, :decimal, precision: 18, scale: 12
  end
end
