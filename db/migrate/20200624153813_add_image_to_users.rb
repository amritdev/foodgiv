# frozen_string_literal: true

class AddImageToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :image_id, :bigint, null: true
    add_foreign_key :users, :images, column: :image_id
  end
end
