# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_07_045818) do

  create_table "allergens", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_allergens_on_name", unique: true
  end

  create_table "foods", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "user_id"
    t.string "title", null: false
    t.text "description"
    t.string "food_type", null: false
    t.integer "quantity", default: 1
    t.string "pickup_time"
    t.datetime "exp_date"
    t.bigint "location_id"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "listed_at"
    t.index ["location_id"], name: "index_foods_on_location_id"
    t.index ["user_id"], name: "index_foods_on_user_id"
  end

  create_table "foods_allergens", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "food_id"
    t.bigint "allergen_id"
    t.index ["allergen_id"], name: "index_foods_allergens_on_allergen_id"
    t.index ["food_id"], name: "index_foods_allergens_on_food_id"
  end

  create_table "foods_images", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "food_id"
    t.bigint "image_id"
    t.index ["food_id"], name: "index_foods_images_on_food_id"
    t.index ["image_id"], name: "index_foods_images_on_image_id"
  end

  create_table "foods_labels", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "food_id"
    t.bigint "label_id"
    t.index ["food_id"], name: "index_foods_labels_on_food_id"
    t.index ["label_id"], name: "index_foods_labels_on_label_id"
  end

  create_table "images", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.text "image_data", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "labels", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_labels_on_name", unique: true
  end

  create_table "locations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.string "place_id"
    t.decimal "lat", precision: 18, scale: 12, null: false
    t.decimal "lng", precision: 18, scale: 12, null: false
    t.string "formatted_address", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "sender_id", null: false
    t.bigint "receiver_id", null: false
    t.bigint "request_id"
    t.bigint "food_id"
    t.text "content"
    t.string "content_type", default: "TEXT"
    t.datetime "read_at"
    t.boolean "sender_archived", default: false
    t.boolean "receiver_archived", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["food_id"], name: "index_messages_on_food_id"
    t.index ["receiver_id"], name: "fk_rails_67c67d2963"
    t.index ["request_id"], name: "index_messages_on_request_id"
    t.index ["sender_id"], name: "fk_rails_b8f26a382d"
  end

  create_table "requests", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "giver_id", null: false
    t.bigint "receiver_id", null: false
    t.bigint "food_id"
    t.text "message"
    t.datetime "accepted_at"
    t.datetime "declined_at"
    t.datetime "cancelled_at"
    t.datetime "completed_at"
    t.boolean "read", default: false
    t.boolean "giver_archived", default: false
    t.boolean "receiver_archived", default: false
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["food_id"], name: "index_requests_on_food_id"
    t.index ["giver_id"], name: "fk_rails_1e71cbdce2"
    t.index ["receiver_id"], name: "fk_rails_6db4392e5e"
  end

  create_table "reviews", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.bigint "request_id", null: false
    t.bigint "food_id", null: false
    t.bigint "giver_id", null: false
    t.bigint "receiver_id", null: false
    t.bigint "reviewer_id", null: false
    t.text "rating_detail", null: false
    t.float "rating_overall", null: false
    t.string "review_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["food_id"], name: "index_reviews_on_food_id"
    t.index ["giver_id"], name: "fk_rails_63ae608b29"
    t.index ["receiver_id"], name: "fk_rails_6f03dedc1c"
    t.index ["request_id"], name: "index_reviews_on_request_id"
    t.index ["reviewer_id"], name: "fk_rails_007031d9cb"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.string "first_name", default: "", null: false
    t.string "last_name"
    t.string "email"
    t.string "phone"
    t.string "password_digest"
    t.string "verification_code"
    t.boolean "phone_verified", default: false
    t.text "summary"
    t.text "favorite_food"
    t.text "hated_food"
    t.bigint "location_id"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "current_sign_in_at"
    t.string "stripe_customer_id"
    t.string "stripe_subscription_id"
    t.string "google_user_id"
    t.string "facebook_user_id"
    t.integer "giver_review_count", default: 0
    t.float "giver_rating", default: 0.0
    t.integer "receiver_review_count", default: 0
    t.float "receiver_rating", default: 0.0
    t.json "device_info"
    t.boolean "notification_allowed", default: false
    t.string "notification_frequency"
    t.integer "notification_distance", default: 0
    t.string "status", default: "ACTIVE"
    t.datetime "created_at", null: false
    t.bigint "image_id"
    t.index ["image_id"], name: "fk_rails_47c7c64b36"
    t.index ["location_id"], name: "index_users_on_location_id"
  end

  add_foreign_key "messages", "users", column: "receiver_id"
  add_foreign_key "messages", "users", column: "sender_id"
  add_foreign_key "requests", "users", column: "giver_id"
  add_foreign_key "requests", "users", column: "receiver_id"
  add_foreign_key "reviews", "users", column: "giver_id"
  add_foreign_key "reviews", "users", column: "receiver_id"
  add_foreign_key "reviews", "users", column: "reviewer_id"
  add_foreign_key "users", "images"
  add_foreign_key "users", "locations"
end
