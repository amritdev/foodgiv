# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

RSpec.describe AppearanceHelper, type: :helper do
  before do
    Rails.cache.clear
  end

  let(:room) { 'test' }
  let(:user) { create(:user) }

  describe '#register_appearance' do
    it 'push user id to array' do
      helper.register_appearance user: user, room: room
      expect(helper.get_appearance_list(room)).to include(user.id)
    end
  end

  describe '#unregister_appearance' do
    it 'remove given user id from array' do
      helper.register_appearance user: user, room: room
      helper.unregister_appearance user: user, room: room
      expect(helper.get_appearance_list(room)).to eq []
    end
  end

  describe '#online?' do
    it 'returns true if a user is present in given room; false otherwise' do
      expect(helper.online?(user: user, room: room)).to eq false
      helper.register_appearance user: user, room: room
      expect(helper.online?(user: user, room: room)).to eq true
    end
  end
end
