# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

RSpec.describe AuthHelper, type: :helper do
  describe '#http_auth_header' do
    it 'returns Authorization content in http header' do
      headers = build_mock_auth_header 'auth_header_content'
      expect(helper.http_auth_header(headers)).to eq('auth_header_content')
    end

    it 'returns empty string when Authorization is missing in header' do
      headers = {
        'Content-Type' => 'application/json'
      }
      expect(helper.http_auth_header(headers)).to eq('')
    end
  end

  describe '#get_decoded_auth_token' do
    it 'returns decoded auth token' do
      user_id = 123
      token = JsonWebToken.encode(user_id)
      expect(helper.get_decoded_auth_token(token)).to eq(user_id)
    end

    it 'returns nil when auth token is invalid' do
      payload = {
        data: 'any',
        exp: Time.now.to_i - 1
      }
      token = jwt_sign_without_lib payload
      expect(helper.get_decoded_auth_token(token)).to eq(nil)
    end
  end

  describe '#get_current_user_from' do
    it 'can get user from header' do
      user = create(:user)
      headers = build_mock_auth_header(JsonWebToken.encode(user.id))
      expect(helper.get_current_user_from(headers: headers).id).to eq(user.id)
    end

    it 'can get user from request param' do
      user = create(:user)
      helper.request.params[:token] = JsonWebToken.encode(user.id)
      expect(helper.get_current_user_from(request: helper.request).id).to eq(user.id)
    end

    it 'returns nil if token is invalid' do
      headers = build_mock_auth_header 'invalid_token'
      expect(helper.get_current_user_from(headers: headers)).to eq(nil)
    end

    it 'returns nil if token is expired' do
      user = create(:user)
      payload = {
        data: user.id,
        exp: Time.now.to_i - 1
      }
      token = jwt_sign_without_lib payload
      headers = build_mock_auth_header token
      expect(helper.get_current_user_from(headers: headers)).to eq(nil)
    end
  end
end
