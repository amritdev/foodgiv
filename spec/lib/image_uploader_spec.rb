# frozen_string_literal: true

require 'spec_helper'
require 'image_uploader'

RSpec.describe ImageUploader do
  let(:model) { Image.create(image: File.open('app/assets/images/test/test.jpg', 'rb')) }
  let(:image) { model.image }
  let(:derivatives) { model.image_derivatives! }

  it 'extracts metadata' do
    expect(image.mime_type).to eq('image/jpeg')
    expect(image.extension).to eq('jpg')
    expect(image.size).to be_instance_of(Integer)
    expect(image.width).to be_instance_of(Integer)
    expect(image.height).to be_instance_of(Integer)
  end

  it 'generates derivatives' do
    expect(derivatives[:small]).to be_kind_of(Shrine::UploadedFile)
    expect(derivatives[:medium]).to be_kind_of(Shrine::UploadedFile)
    expect(derivatives[:large]).to be_kind_of(Shrine::UploadedFile)
  end

  it 'makes image trait work correctly' do
    expect(model.small).to be_present
  end
end
