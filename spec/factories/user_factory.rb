# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    first_name { Faker::Name.unique.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.unique.email }
    password { Faker::Internet.password(min_length: 8, max_length: 50) }
    phone { Faker::PhoneNumber.cell_phone_in_e164 }
    phone_verified { true }
    summary { Faker::Lorem.paragraph(sentence_count: 3) }
    favorite_food { Faker::Number.between(from: 0, to: 5).times.map { |_i| Faker::Food.dish }.join(',') }
    hated_food { Faker::Number.between(from: 0, to: 3).times.map { |_i| Faker::Food.dish }.join(',') }
    association :profile_image, factory: :image
    location
    status { 'ACTIVE' }
    created_at { DateTime.now }

    trait :no_location do
      location { nil }
    end

    trait :active do
      status { 'ACTIVE' }
    end

    trait :disabled do
      status { 'DISABLED' }
    end

    trait :deleted do
      status { 'DELETED' }
    end

    trait :subscribed do
      stripe_customer_id { 'cus_' + Faker::Alphanumeric.alpha(number: 14) }
      stripe_subscription_id { 'sub_' + Faker::Alphanumeric.alphanumeric(number: 14) }
    end

    trait :with_device do
      device_info do
        { device_type: 'ANDROID', device_token: 'dummy_token' }
      end
    end

    trait :notification_subscribed do
      subscribed
      with_device
      notification_allowed do
        true
      end
      notification_distance do
        10
      end
      notification_frequency do
        'INSTANT'
      end
    end
  end
end
