# frozen_string_literal: true

FactoryBot.define do
  factory :request do
    food
    giver { food.user }
    association :receiver, factory: %i[user subscribed]
    message { Faker::Lorem.sentence }
    accepted_at { nil }
    declined_at { nil }
    read { false }
    giver_archived { false }
    receiver_archived { false }
    status { 'NEW' }

    trait :accepted do
      food { build(:food, status: 'ACCEPTED') }
      accepted_at { DateTime.now }
      status { 'ACCEPTED' }
    end

    trait :declined do
      declined_at { DateTime.now }
      status { 'DECLINED' }
    end

    trait :cancelled do
      cancelled_at { DateTime.now }
      status { 'CANCELLED' }
    end

    trait :completed do
      food { build(:food, status: 'COMPLETED') }
      accepted_at { DateTime.now }
      completed_at { DateTime.now }
      status { 'COMPLETED' }
    end

    trait :deleted do
      status { 'DELETED' }
    end

    trait :has_read do
      read { true }
    end

    trait :archived do
      giver_archived { true }
      receiver_archived { true }
    end
  end
end
