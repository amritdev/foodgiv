# frozen_string_literal: true

def generate_rating_detail(type = 'FOR_GIVER')
  detail = if type == 'FOR_GIVER'
             {
               food_quality: Faker::Number.between(from: 0, to: 5),
               pickup_exp: Faker::Number.between(from: 0, to: 5)
             }
           else
             {
               collector_exchange_exp: Faker::Number.between(from: 0, to: 5)
             }
           end
  detail[:feedback] = Faker::Lorem.sentence
  JSON.generate detail
end

def calc_rating_ovr(detail)
  detail = JSON.parse detail
  if detail['collector_exchange_exp'].nil?
    (detail['food_quality'] + detail['pickup_exp']) / 2
  else
    detail['collector_exchange_exp']
  end
end

FactoryBot.define do
  factory :review do
    request
    food { request.food }
    giver { request.giver }
    receiver { request.receiver }
    reviewer { request.receiver }
    review_type { 'FOR_GIVER' }
    rating_detail { generate_rating_detail review_type }
    rating_overall { calc_rating_ovr rating_detail }

    trait :for_giver do
      reviewer { request.receiver }
      review_type { 'FOR_GIVER' }
    end

    trait :for_receiver do
      reviewer { request.giver }
      review_type { 'FOR_RECEIVER' }
    end
  end
end
