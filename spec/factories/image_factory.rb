# frozen_string_literal: true

require_relative '../spec_main_helper'

include SpecMainHelper

FactoryBot.define do
  factory :image do
    image_data { shrine_image_data('test.jpg') }
  end

  trait :avatar do
    image_data { shrine_image_data('avatar.jpg') }
  end

  trait :food do
    image_data { shrine_image_data('food.jpg') }
  end
end
