# frozen_string_literal: true

FactoryBot.define do
  factory :food do
    user
    title { Faker::Food.dish }
    description { Faker::Food.description }
    quantity { Faker::Number.between(from: 1, to: 20) }
    pickup_time { Faker::Time.between(from: DateTime.now + 1, to: DateTime.now + 2, format: :short) }
    exp_date { Faker::Time.between_dates(from: Date.today + 1, to: Date.today + 10, format: :default) }
    food_type { 'PREPARED' }
    location
    status { 'LISTED' }
    listed_at { DateTime.now }

    transient do
      labels_count { Faker::Number.between(from: 0, to: 5) }
      allergens_count { Faker::Number.between(from: 0, to: 5) }
      images_count { Faker::Number.between(from: 0, to: 5) }
    end

    trait :prepared do
      food_type { 'PREPARED' }
      title { Faker::Food.dish }
    end

    trait :unprepared do
      food_type { 'UNPREPARED' }
      title { Faker::Food.vegetables }
    end

    trait :listed do
      status { 'LISTED' }
    end

    trait :unlisted do
      status { 'UNLISTED' }
      listed_at { nil }
    end

    trait :deleted do
      status { 'DELETED' }
    end

    trait :expired do
      exp_date { DateTime.now - Faker::Number.between(from: 0, to: 72).hours }
      status { 'LISTED' }
    end

    trait :failed do
      status { 'FAILED' }
    end

    trait :accepted do
      status { 'ACCEPTED' }
    end

    trait :declined do
      status { 'DECLINED' }
    end

    trait :completed do
      status { 'COMPLETED' }
    end

    after(:create) do |food, evaluator|
      create_list(:label, evaluator.labels_count, foods: [food])
      create_list(:allergen, evaluator.allergens_count, foods: [food])
      create_list(:image, evaluator.images_count, :food, foods: [food])
      if food.status == 'ACCEPTED'
        create(:request, :accepted, food: food)
      elsif food.status == 'DECLINED'
        create(:request, :declined, food: food)
      elsif food.status == 'COMPLETED'
        create(:request, :completed, food: food)
      end
    end
  end
end
