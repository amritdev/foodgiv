# frozen_string_literal: true

FactoryBot.define do
  factory :message do
    request
    food { request.food }
    sender { request.giver }
    receiver { request.receiver }
    content { Faker::Lorem.sentence }
    content_type { 'TEXT' }
    sender_archived { false }
    receiver_archived { false }

    trait :read do
      read_at { DateTime.now }
    end

    trait :giver_message do
      sender { request.giver }
      receiver { request.receiver }
    end

    trait :receiver_message do
      sender { request.receiver }
      receiver { request.giver }
    end

    trait :sender_archived do
      sender_archived { true }
    end

    trait :receiver_archived do
      receiver_archived { true }
    end
  end
end
