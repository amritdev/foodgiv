# frozen_string_literal: true

FactoryBot.define do
  factory :label do
    name { Faker::Lorem.unique.characters(number: Faker::Number.between(from: 4, to: 10)) }
  end
end
