# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

RSpec.describe 'Images', type: :request do
  let(:user) { create(:user) }
  let(:file) { Rack::Test::UploadedFile.new(Rails.root.join('app', 'assets', 'images', 'test', 'test.jpg'), 'image/jpeg') }

  describe '#new' do
    context 'when jwt token is invalid' do
      it 'returns code AUTHENTICATION_FAILED' do
        post '/images/new', params: { image: file }, headers: build_mock_auth_header('token_that_never_works')
        json = JSON.parse response.body
        expect(json['success']).to eq false
        expect(json['msg']).to eq 'AUTHENTICATION_FAILED'
      end
    end

    context 'when jwt token is valid' do
      it 'uploads image' do
        post '/images/new', params: { image: file }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        expect(json['success']).to eq true
        expect(json['data']['id']).to be_present
        expect(json['data']['image']).to be_present
        expect(json['data']['small']).to be_present
        image = Image.find(json['data']['id'])
        expect(image.image).to be_present
        expect(image.image(:large)).to be_present
        expect(image.image(:medium)).to be_present
        expect(image.image(:small)).to be_present
        expect(image.image(:small).original_filename).to be_present
        expect(image.image(:small).size).to be_present
        expect(image.image(:small).width).to be_present
        expect(image.image(:small).height).to be_present
        expect(image.image(:small).url).to be_present
      end
    end
  end

  describe '#multiple_upload' do
    context 'when jwt token is invalid' do
      it 'returns code AUTHENTICATION_FAILED' do
        post '/images/multiple-upload', params: { image: file }, headers: build_mock_auth_header('token_that_never_works')
        json = JSON.parse response.body
        expect(json['success']).to eq false
        expect(json['msg']).to eq 'AUTHENTICATION_FAILED'
      end
    end

    context 'when jwt token is valid' do
      it 'uploads multiple images' do
        post '/images/multiple-upload', params: { images: { "0": file } }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        expect(json['success']).to eq true
        expect(json['data']).to be_an_instance_of(Array)
        expect(json['data'].size).to eq 1
      end
    end
  end
end
