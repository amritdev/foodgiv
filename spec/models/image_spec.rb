# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Image, type: :model do
  describe 'factory' do
    let(:image) { create(:image) }

    it 'creates an image' do
      expect(image.image).to be_present
      expect(image.image(:large)).to be_present
      expect(image.image(:medium)).to be_present
      expect(image.image(:small)).to be_present
      expect(image.image(:small).original_filename).to be_present
      expect(image.image(:small).size).to be_present
      expect(image.image(:small).width).to be_present
      expect(image.image(:small).height).to be_present
      expect(image.image(:small).url).to be_present
    end

    it 'implements trait for size correctly' do
      expect(image.small).to be_present
    end
  end
end
