# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

RSpec.describe Message, type: :model do
  describe 'factory' do
    it 'creates a message' do
      message = create(:message)
      expect(message.id).to be_present
      expect(message.sender.id).to be_present
      expect(message.receiver.id).to be_present
      expect(message.request.id).to be_present
      expect(message.food.id).to be_present
      expect(message.content).to be_present
      expect(message.content_type).to be_present
      expect(message.request.food.id).to eq message.food.id
    end

    context 'when trait is read' do
      it 'creates a message that has been read by receiver' do
        message = create(:message, :read)
        expect(message.read_at).to be_present
        expect(message.read?).to eq true
      end
    end
  end

  describe '#where_belongs_to' do
    it 'returns messages belong to a user' do
      message_1 = create(:message)
      message_2 = create(:message, sender: message_1.sender)
      message_3 = create(:message)
      expect(described_class.where_belongs_to(message_1.sender).count).to eq 2
      expect(described_class.where_belongs_to(message_1.receiver).count).to eq 1
    end
  end

  describe '#archived' do
    it 'returns archived messages belong to a user' do
      message_1 = create(:message)
      message_2 = create(:message, sender: message_1.sender, sender_archived: true)
      message_3 = create(:message, receiver: message_2.receiver, receiver_archived: true)
      message_4 = create(:message, receiver: message_2.receiver, receiver_archived: true)
      expect(described_class.archived(message_1.sender).count).to eq 1
      expect(described_class.archived(message_2.receiver).count).to eq 2
    end
  end

  describe '#unarchived' do
    it 'returns unarchived messages belong to a user' do
      message_1 = create(:message)
      message_2 = create(:message, sender: message_1.sender, sender_archived: true)
      message_3 = create(:message, receiver: message_2.receiver, receiver_archived: true)
      message_4 = create(:message, receiver: message_2.receiver, receiver_archived: true)
      message_5 = create(:message, sender: message_1.sender, receiver: message_1.receiver)
      expect(described_class.unarchived(message_1.sender).count).to eq 2
      expect(described_class.unarchived(message_2.receiver).count).to eq 1
      expect(described_class.unarchived(message_1.receiver).count).to eq 2
    end
  end

  describe '#unread' do
    it 'returns unread messages by user' do
      message = create(:message)
      message_read = create(:message, :read, food: message.food, receiver: message.receiver)
      message_unread = create(:message, food: message.food, receiver: message.receiver)
      expect(described_class.unread(message.receiver).count).to eq 2
      message_new = create(:message, food: message.food, receiver: message.receiver)
      expect(described_class.unread(message.receiver).count).to eq 3
      message_archived = create(:message, :receiver_archived, food: message.food, receiver: message.receiver)
      expect(described_class.unread(message.receiver).count).to eq 3
    end
  end

  describe '#get_relationship' do
    it 'returns correct relationship' do
      message = create(:message)
      expect(message.get_relationship(message.sender)).to eq 'SENDER'
      expect(message.get_relationship(message.receiver)).to eq 'RECEIVER'
      expect(message.get_relationship(create(:user))).to eq 'UNKNOWN'
      expect(message.get_relationship(nil)).to eq 'UNKNOWN'
    end
  end

  describe '#archived?' do
    it 'returns correct archive status' do
      message = create(:message, :sender_archived)
      expect(message.archived?(message.sender)).to eq true
      message = create(:message, :receiver_archived)
      expect(message.archived?(message.receiver)).to eq true
    end
  end
end
