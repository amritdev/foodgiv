# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

RSpec.describe Food, type: :model do
  describe 'factory' do
    it 'creates a food' do
      food = create(:food)
      expect(food[:title]).to be_present
      expect(food[:description]).to be_present
      expect(food[:food_type]).to be_present
      expect(food[:quantity]).to be_present
      expect(food[:pickup_time]).to be_present
      expect(food[:exp_date]).to be_present
      expect(food[:location_id]).to be_present
      expect(food.images.to_a).to be_kind_of(Array)
    end

    context 'when trait is expired' do
      it 'creates an expired food' do
        food = create(:food, :expired)
        expect(food.expired?).to eq true
      end
    end
  end

  it 'acts as mappable' do
    loc_nearest = build(:location, lat: 1, lng: 1)
    loc_near = build(:location, lat: 0, lng: 0)
    loc_farthest = build(:location, lat: 5, lng: 5)
    food_near = create(:food, title: 'near', location: loc_near)
    food_farthest = create(:food, title: 'farthest', location: loc_farthest)
    food_nearest = create(:food, title: 'nearest', location: loc_nearest)
    foods = described_class.joins(:location).by_distance(origin: [1, 1])
    expect(foods.first.title).to eq('nearest')
    expect(foods.second.title).to eq('near')
    expect(foods.third.title).to eq('farthest')
  end

  describe '#expired?' do
    it 'returns true if expired date is equal to or bigger than now' do
      [DateTime.now, DateTime.now - 1.hour].each do |time|
        expect(create(:food, exp_date: time).expired?).to eq true
      end
    end

    it 'returns false if expired date is less than now' do
      expect(create(:food, exp_date: DateTime.now + 1.hour).expired?).to eq false
    end
  end

  describe '#request_count' do
    it 'returns requests count' do
      food = create(:food)
      expect(food.request_count).to eq 0
      request_1 = create(:request, food: food)
      request_2 = create(:request, food: food)
      food.reload
      expect(food.request_count).to eq 2
    end
  end

  describe '#message_count' do
    it 'returns message count' do
      food = create(:food)
      expect(food.message_count).to eq 0
      message_1 = create(:message, food: food)
      message_2 = create(:message, food: food)
      message_3 = create(:message, food: food)
      food.reload
      expect(food.message_count).to eq 3
    end
  end

  describe '#where_has_requests' do
    it 'returns foods that has requests' do
      food = create(:food)
      request = create(:request)
      expect(described_class.where_has_requests.all.count).to eq 1
      expect(described_class.where_has_requests.first.id).to eq request.food.id
    end
  end

  describe '#where_has_requests_belongs_to_user' do
    it 'returns foods that has requests belong to user' do
      user = create(:user)
      food_1 = create(:food, user: user)
      food_2 = create(:food)
      food_3 = create(:food)
      request_1 = create(:request, food: food_1)
      request_2 = create(:request, food: food_2, receiver: user)
      request_3 = create(:request, food: food_3)
      expect(described_class.where_has_requests_belongs_to_user(user).all.count).to eq 2
    end
  end

  describe '#last_action_at' do
    context 'when there is no request' do
      it 'returns created_at' do
        food = create(:food)
        expect(food.last_action_at).to eq food.created_at
      end
    end

    context 'when there are requests' do
      it 'returns latest last_action_at of requests' do
        food = create(:food)
        request_1 = create(:request, food: food, created_at: DateTime.now - 2.hours)
        request_2 = create(:request, food: food, created_at: DateTime.now - 1.hour)
        expect(food.last_action_at).to eq request_2.last_action_at
      end
    end
  end

  describe '#lately_listed_in' do
    it 'returns foods that listed in given time ago' do
      food_1 = create(:food, created_at: DateTime.now - 2.hours)
      food_2 = create(:food, created_at: DateTime.now - 1.hour)
      food_3 = create(:food)
      food_4 = create(:food, :expired, created_at: DateTime.now)
      food_5 = create(:food, :completed)
      food_6 = create(:food, :unlisted)
      recent_foods = described_class.lately_listed_in 90.minutes
      expect(recent_foods.count).to eq 2
      expect(recent_foods.all.to_a).to include(food_2, food_3)
    end
  end
end
