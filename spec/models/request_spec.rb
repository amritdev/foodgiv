# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Request, type: :model do
  let(:user) { create(:user) }
  let(:request) { create(:request) }

  describe 'factory' do
    it 'creates a request' do
      request = create(:request)
      expect(request.id).to be_present
      expect(request.giver.id).to be_present
      expect(request.receiver.id).to be_present
      expect(request.food.id).to be_present
      # request giver id should be equal to food user id
      expect(request.giver.id).to eq request.food.user.id
    end

    it 'has has_one relationship with review_give' do
      request = create(:request, :completed)
      review = create(:review, :for_giver, request: request)
      expect(request.review_give.id).to eq review.id
      expect(request.review_give.reviewer.id).to eq request.receiver.id
    end

    it 'has has_one relationship with review_receive' do
      request = create(:request, :completed)
      review = create(:review, :for_receiver, request: request)
      expect(request.review_receive.id).to eq review.id
      expect(request.review_receive.reviewer.id).to eq request.giver.id
    end
  end

  describe '#giver_id?' do
    it 'returns true if given id is giver id; false otherwise' do
      request = create(:request)
      expect(request.giver_id?(request.giver.id)).to eq true
      expect(request.giver_id?(create(:user).id)).to eq false
    end
  end

  describe '#giver?' do
    it 'returns true if given user is giver; false otherwise' do
      request = create(:request)
      expect(request.giver?(request.giver)).to eq true
      expect(request.giver?(create(:user))).to eq false
    end
  end

  describe '#receiver_id?' do
    it 'returns true if given id is receiver id; false otherwise' do
      request = create(:request)
      expect(request.receiver_id?(request.receiver.id)).to eq true
      expect(request.receiver_id?(create(:user).id)).to eq false
    end
  end

  describe '#receiver?' do
    it 'returns true if given user is receiver; false otherwise' do
      request = create(:request)
      expect(request.receiver?(request.receiver)).to eq true
      expect(request.receiver?(create(:user))).to eq false
    end
  end

  describe '#get_relationship' do
    it 'returns GIVER if given user is giver, RECEIVER if user is reciever; UNKNOWN otherwise' do
      request = create(:request)
      expect(request.get_relationship(request.giver)).to eq 'GIVER'
      expect(request.get_relationship(request.receiver)).to eq 'RECEIVER'
      expect(request.get_relationship(create(:user))).to eq 'UNKNOWN'
    end
  end

  describe '#get_other_party' do
    it 'returns giver if given user is receiver, receiver if user is giver; nil otherwise' do
      request = create(:request)
      expect(request.get_other_party(request.giver).id).to eq request.receiver.id
      expect(request.get_other_party(request.receiver).id).to eq request.giver.id
      expect(request.get_other_party(create(:user))).to be_nil
    end
  end

  describe '#where_belongs_to' do
    it 'returns requests belongs to a user' do
      request_1 = create(:request)
      request_2 = create(:request, giver: request_1.giver)
      request_3 = create(:request, receiver: request_1.giver)
      expect(described_class.where_belongs_to(request_1.giver).count).to eq 3
      expect(described_class.where_belongs_to(request_1.receiver).count).to eq 1
    end
  end

  describe '#archived' do
    it 'returns requests archived by user' do
      request_1 = create(:request)
      request_2 = create(:request, giver: request_1.giver, giver_archived: true)
      expect(described_class.archived(request_1.giver).first.id).to eq request_2.id
    end
  end

  describe '#archived?' do
    it 'returns true when archived by user; false otherwise' do
      request = create(:request, giver_archived: true)
      expect(request.archived?(request.giver)).to eq true
      expect(request.archived?(request.receiver)).to eq false
    end
  end

  describe '#unarchived' do
    it "returns user's requests not archived" do
      request_1 = create(:request)
      request_2 = create(:request, giver: request_1.giver, giver_archived: true)
      expect(described_class.unarchived(request_1.giver).first.id).to eq request_1.id
    end
  end

  describe '#last_message' do
    it 'returns last message' do
      request = create(:request)
      message_1 = create(:message, request: request)
      message_2 = create(:message, request: request)
      expect(request.last_message.id).to eq message_2.id
    end
  end

  describe '#last_action_at' do
    context 'when there is no message' do
      it 'returns last action date' do
        request = create(:request)
        expect(request.last_action_at).to eq request.updated_at
      end
    end

    context 'when there are messages' do
      it 'returns last message created_at' do
        request = create(:request)
        message_1 = create(:message, request: request, created_at: request.created_at + 1.hour)
        message_2 = create(:message, request: request, created_at: request.created_at + 2.hours)
        expect(request.last_action_at).to eq message_2.created_at
      end
    end
  end

  describe '#where_no_review_by' do
    it 'returns requests where review was not left by given user' do
      request_1 = create(:request, giver: user)
      request_2 = create(:request, receiver: user)
      request_3 = create(:request)
      request_4 = create(:request, giver: user)
      request_5 = create(:request, receiver: user)
      request_6 = create(:request, giver: user)
      review_1 = create(:review, :for_receiver, request: request_1)
      review_2 = create(:review, :for_giver, request: request_2)
      review_3 = create(:review, :for_giver, request: request_4)
      review_4 = create(:review, :for_receiver, request: request_5)
      result = described_class.where_no_review_by(user)
      expect(result.all.count).to eq 3
      expect(result.all.collect(&:id)).to include(
        request_4.id, request_5.id, request_6.id
      )
    end
  end

  describe '#where_review_created_by' do
    it 'returns requests that review created by given user' do
      request_1 = create(:request, giver: user)
      request_2 = create(:request, receiver: user)
      request_3 = create(:request)
      request_4 = create(:request, giver: user)
      request_5 = create(:request, receiver: user)
      request_6 = create(:request, giver: user)
      review_1 = create(:review, :for_receiver, request: request_1)
      review_2 = create(:review, :for_giver, request: request_2)
      review_3 = create(:review, :for_giver, request: request_4)
      review_4 = create(:review, :for_receiver, request: request_5)
      result = described_class.where_review_created_by(user)
      expect(result.all.count).to eq 2
      expect(result.all.collect(&:id)).to include(
        request_1.id, request_2.id
      )
    end
  end

  describe '#where_review_received_by' do
    it 'returns requests that review received by given user' do
      request_1 = create(:request, giver: user)
      request_2 = create(:request, receiver: user)
      request_3 = create(:request)
      request_4 = create(:request, giver: user)
      request_5 = create(:request, receiver: user)
      request_6 = create(:request, giver: user)
      review_1 = create(:review, :for_receiver, request: request_1)
      review_2 = create(:review, :for_giver, request: request_2)
      review_3 = create(:review, :for_giver, request: request_4)
      review_4 = create(:review, :for_receiver, request: request_5)
      result = described_class.where_review_received_by(user)
      expect(result.all.count).to eq 2
      expect(result.all.collect(&:id)).to include(
        request_4.id, request_5.id
      )
    end
  end
end
