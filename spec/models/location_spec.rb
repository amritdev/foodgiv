# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Location, type: :model do
  describe 'factory' do
    let(:location) { create(:location) }

    it 'creates a location' do
      expect(location[:id]).to be_present
      expect(location[:place_id]).to be_present
      expect(location[:lat]).to be_present
      expect(location[:lng]).to be_present
      expect(location[:formatted_address]).to be_present
    end
  end
  #
  # Cannot test cache clear because memcache does not support delete_matched
  #
  # describe '#after_update' do
  #   it 'clears caculated distance' do
  #     location_1 = create(:location)
  #     location_2 = create(:location)
  #     user_1 = create(:user, location: location_1)
  #     user_2 = create(:user, location: location_2)
  #     user_1.distance_from(location_2)
  #     user_2.distance_from(location_1)
  #     expect(Rails.cache.read("distance/#{location_1.id}/#{location_2.id}")).to be_present
  #     expect(Rails.cache.read("distance/#{location_2.id}/#{location_1.id}")).to be_present
  #     location_1.lat = location_1.lat + 0.01
  #     location_1.save!
  #     puts "checking distance/#{location_1.id}/#{location_2.id}"
  #     expect(Rails.cache.read("distance/#{location_1.id}/#{location_2.id}")).to be_nil
  #     puts "checking distance/#{location_2.id}/#{location_1.id}"
  #     expect(Rails.cache.read("distance/#{location_2.id}/#{location_1.id}")).to be_nil
  #   end
  # end
end
