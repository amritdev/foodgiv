# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'
RSpec.describe User, type: :model do
  describe 'factory' do
    it 'creates a user' do
      user = create(:user)
      expect(user[:id]).to be_present
      expect(user[:first_name]).to be_present
      expect(user[:last_name]).to be_present
      user = create(:user, :disabled)
      expect(user[:status]).to eq('DISABLED')
    end

    it 'creates associations' do
      user = create(:user)
      expect(user.location).to be_present
      expect(user.profile_image).to be_present
      expect(user.profile_image.image).to be_present
    end

    context 'when no location trait is given' do
      it 'does not create location' do
        user = create(:user, :no_location)
        expect(user.location).to be_nil
      end
    end

    describe '#with_device' do
      it 'sets device info' do
        user = create(:user, :with_device)
        expect(user.device_info['device_type']).to be_present
        expect(user.device_info['device_token']).to be_present
      end
    end

    describe '#notification_subscribed' do
      it 'creates a user that allows new listing notification' do
        user = create(:user, :notification_subscribed)
        expect(user.device_info).to be_present
        expect(user.notification_allowed).to eq true
        expect(user.notification_distance).to be_kind_of Numeric
        expect(user.notification_frequency).to be_present
      end
    end
  end

  describe '#where_email_icase' do
    it 'ignores email letter case' do
      user = create(:user)
      expect(described_class.where_email_icase(user.email.upcase).first.id).to eq(user.id)
    end
  end

  describe '#device_info' do
    it 'returns nil when device_info is not set' do
      user = create(:user)
      expect(user.device_info).to be_nil
    end

    it 'returns hash when device_info is set' do
      user = create(:user)
      user.device_info = {
        device_type: 'IOS',
        device_token: 'dummy_token'
      }
      user.save!
      user.reload
      expect(user.device_info).to include(
        'device_type' => 'IOS',
        'device_token' => 'dummy_token'
      )
    end
  end

  describe '#distance_from' do
    it 'returns distance from given location' do
      user_1 = create(:user, location: create(:location, :new_york))
      user_2 = create(:user, location: create(:location, :brooklyn))
      expect(user_1.distance_from(user_2.location)).to be_kind_of Numeric
    end

    it 'stores caculated distance into cache' do
      user = create(:user)
      location = create(:location)
      distance = user.distance_from(location)
      expect(Rails.cache.read("distance:#{user.location.id}-#{location.id}")).to eq distance
    end
  end

  describe '#reviews_received_as_giver' do
    it 'returns reviews received as a giver' do
      user = create(:user)
      request = create(:request, giver: user)
      review_1 = create(:review, request: request, reviewer: user, review_type: 'FOR_RECEIVER')
      review_2 = create(:review, request: request, review_type: 'FOR_GIVER')
      expect(user.reviews_received_as_giver.all.size).to eq 1
      expect(user.reviews_received_as_giver.first).to eq review_2
    end
  end
end
