# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Allergen, type: :model do
  describe 'factory' do
    it 'creates an allergen' do
      allergen = create(:allergen)
      expect(allergen[:id]).to be_present
      expect(allergen[:name]).to be_present
    end
  end
end
