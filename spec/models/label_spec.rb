# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Label, type: :model do
  describe 'factory' do
    it 'creates a label' do
      label = create(:label)
      expect(label[:id]).to be_present
      expect(label[:name]).to be_present
    end
  end
end
