# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Review, type: :model do
  describe 'factory' do
    it 'creates a review' do
      review = create(:review)
      expect(review.request.id).to be_present
      expect(review.food.id).to be_present
      expect(review.giver.id).to be_present
      expect(review.receiver.id).to be_present
      expect(review.reviewer.id).to be_present
      expect(review.rating_detail).to be_present
      expect(review.feedback).to be_present
      expect(review.rating_overall).not_to be_nil
      expect(review.food.user.id).to eq review.giver.id
      expect(review.giver.id).to eq review.request.giver.id
      expect(review.receiver.id).to eq review.request.receiver.id
    end

    context 'when trait is for_giver' do
      it 'creates a review with type FOR_GIVER' do
        review = create(:review, :for_giver)
        expect(review.review_type).to eq 'FOR_GIVER'
        expect(review.food_quality).not_to be_nil
        expect(review.pickup_exp).not_to be_nil
        expect(review.rating_overall).not_to be_nil
      end
    end

    context 'when trait is for_receiver' do
      it 'creates a review with receive type' do
        review = create(:review, :for_receiver)
        expect(review.review_type).to eq 'FOR_RECEIVER'
        expect(review.collector_exchange_exp).not_to be_nil
        expect(review.rating_overall).not_to be_nil
      end
    end
  end
end
