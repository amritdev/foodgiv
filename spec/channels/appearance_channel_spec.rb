# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AppearanceChannel, type: :channel do
  include AppearanceHelper

  let(:user) { create(:user) }

  before do
    stub_connection user_id: user.id
  end

  # describe '#subscribe' do
  #   it 'registers user connection' do
  #     subscribe
  #     expect(get_appearance_list).to eq [user.id]
  #   end
  # end

  # describe '#unsubscribe' do
  #   it 'unregisters user connection' do
  #     unsubscribe
  #     expect(get_appearance_list).to eq []
  #   end
  # end
end
