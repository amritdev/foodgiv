# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MessageBroadcastJob, type: :job do
  describe '#perform' do
    it 'broadcasts messages' do
      described_class.perform_now(message: create(:message))
    end
  end
end
