# frozen_string_literal: true

require 'rails_helper'

def labels_query
  <<~GQL
    query{
      labels {
        id
        name
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    describe '#labels' do
      it('returns labels') do
        create(:label, name: 'foo')
        post '/graphql', params: { query: labels_query }
        json = JSON.parse(response.body)
        data = json['data']['labels']
        label = data.find { |d| d['name'] == 'foo' }
        expect(label).to be_truthy
        expect(label['id'].present?).to be true
      end
    end
  end
end
