# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def build_requests_connection_query_param(show_archived: false, food_id: nil)
  return '' if show_archived.nil?

  <<~PARAM
        (
          filter: {
            showArchived: #{show_archived ? 'true' : 'false'}
            #{if food_id.nil?
                ''
              else
                "
            foodId: #{food_id}
            "
    end
            }
          }
        )
  PARAM
end

def requests_connection_query(show_archived: false, food_id: nil)
  <<~GQL
    query {
      requestsConnection #{build_requests_connection_query_param(show_archived: show_archived, food_id: food_id)} {
        pageInfo {
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
        }
        totalCount
        nodes {
          id
          giver {
            id
          }
          receiver {
            id
          }
          food {
            id
          }
          message
          status
          acceptedAt
          declinedAt
          cancelledAt
          completedAt
          read
          giverArchived
          receiverArchived
          lastMessage {
            id
          }
          unreadMessageCount
        }
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    describe '#requests_connection' do
      it 'returns requests created by a user' do
        user = create(:user)
        request_1 = create(:request, giver: user)
        request_2 = create(:request, giver: user)
        request_3 = create(:request, receiver: user)
        request_4 = create(:request)
        post '/graphql', params: { query: requests_connection_query }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        expect(json['data']['requestsConnection']['totalCount']).to eq 3
        expect(json['data']['requestsConnection']['nodes'][0]['id']).to eq request_3.id.to_s
        request_5 = create(:request, receiver: user, food: request_1.food)
        post '/graphql', params: { query: requests_connection_query(food_id: request_1.food.id) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        expect(json['data']['requestsConnection']['totalCount']).to eq 2
      end

      it 'can filter by food id' do
        user = create(:user)
        food = create(:food, user: user)
        request_1 = create(:request, food: food)
        request_2 = create(:request, food: food)
        request_3 = create(:request, food: food)
        request_4 = create(:request)
        request_5 = create(:request)
        post '/graphql', params: { query: requests_connection_query(food_id: food.id) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        expect(json['data']['requestsConnection']['totalCount']).to eq 3
      end

      it 'includes archived messages when show_archived is true' do
        user = create(:user)
        food = create(:food, user: user)
        request_1 = create(:request, food: food)
        request_2 = create(:request, food: food)
        request_3 = create(:request, food: food, giver_archived: true)
        post '/graphql', params: { query: requests_connection_query(food_id: food.id) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        expect(json['data']['requestsConnection']['totalCount']).to eq 2
        post '/graphql', params: { query: requests_connection_query(food_id: food.id, show_archived: true) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        expect(json['data']['requestsConnection']['totalCount']).to eq 3
      end

      it 'includes last message' do
        request = create(:request)
        message = create(:message, request: request)
        post '/graphql', params: { query: requests_connection_query }, headers: get_auth_header_from(request.giver)
        json = JSON.parse response.body
        expect(json['data']['requestsConnection']['nodes'][0]['lastMessage']['id']).to eq(message.id.to_s)
      end

      it 'includes unread message count' do
        request = create(:request)
        message = create(:message, request: request, sender: request.receiver, receiver: request.giver)
        post '/graphql', params: { query: requests_connection_query }, headers: get_auth_header_from(request.giver)
        json = JSON.parse response.body
        expect(json['data']['requestsConnection']['nodes'][0]['unreadMessageCount']).to eq 1
        message.read_at = DateTime.now
        message.save!
        post '/graphql', params: { query: requests_connection_query }, headers: get_auth_header_from(request.giver)
        json = JSON.parse response.body
        expect(json['data']['requestsConnection']['nodes'][0]['unreadMessageCount']).to eq 0
      end
    end
  end
end
