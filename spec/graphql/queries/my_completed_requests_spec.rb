# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def build_my_completed_requests_param(type)
  return '' if type.blank?

  <<~PARAM
    (
      type: #{type}
    )
  PARAM
end

def my_completed_requests_query(type = nil)
  <<~GQL
    query {
      myCompletedRequestsConnection #{build_my_completed_requests_param(type)} {
        totalCount
        nodes {
          id
          giver {
            id
          }
          receiver {
            id
          }
          reviewGive {
            id
            reviewer {
              id
            }
          }
          reviewReceive {
            id
            reviewer {
              id
            }
          }
        }
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    let(:user) { create(:user) }

    it 'returns completed request belong to user' do
      request_1 = create(:request, :completed, giver: user)
      request_2 = create(:request, :completed, receiver: user)
      request_3 = create(:request, giver: user)
      request_4 = create(:request, :completed)
      request_5 = create(:request)
      post '/graphql', params: { query: my_completed_requests_query }, headers: get_auth_header_from(user)
      json = JSON.parse response.body
      expect(json['data']['myCompletedRequestsConnection']['totalCount']).to eq 2
      expect(json['data']['myCompletedRequestsConnection']['nodes'].collect { |n| n['id'] }).to include(
        request_1.id.to_s,
        request_2.id.to_s
      )
    end

    it 'filters by request give or receive type' do
      request_1 = create(:request, :completed, giver: user)
      request_2 = create(:request, :completed, receiver: user)
      request_3 = create(:request, :completed)
      request_4 = create(:request, :completed, giver: user)
      request_5 = create(:request, :completed, receiver: user)
      request_6 = create(:request, giver: user)
      review_1 = create(:review, :for_receiver, request: request_1)
      review_2 = create(:review, :for_giver, request: request_2)
      review_3 = create(:review, :for_giver, request: request_4)
      review_4 = create(:review, :for_receiver, request: request_5)

      post '/graphql', params: { query: my_completed_requests_query('GIVE') }, headers: get_auth_header_from(user)
      json = JSON.parse response.body
      expect(json['data']['myCompletedRequestsConnection']['totalCount']).to eq 2
      expect(json['data']['myCompletedRequestsConnection']['nodes'].collect { |n| n['id'] }).to include(
        request_1.id.to_s, request_4.id.to_s
      )

      post '/graphql', params: { query: my_completed_requests_query('RECEIVE') }, headers: get_auth_header_from(user)
      json = JSON.parse response.body
      expect(json['data']['myCompletedRequestsConnection']['totalCount']).to eq 2
      expect(json['data']['myCompletedRequestsConnection']['nodes'].collect { |n| n['id'] }).to include(
        request_2.id.to_s, request_5.id.to_s
      )

      post '/graphql', params: { query: my_completed_requests_query('REVIEW_NOT_CREATED') }, headers: get_auth_header_from(user)
      json = JSON.parse response.body
      expect(json['data']['myCompletedRequestsConnection']['totalCount']).to eq 2
      expect(json['data']['myCompletedRequestsConnection']['nodes'].collect { |n| n['id'] }).to include(
        request_4.id.to_s, request_5.id.to_s
      )

      post '/graphql', params: { query: my_completed_requests_query('REVIEW_CREATED') }, headers: get_auth_header_from(user)
      json = JSON.parse response.body
      expect(json['data']['myCompletedRequestsConnection']['totalCount']).to eq 2
      expect(json['data']['myCompletedRequestsConnection']['nodes'].collect { |n| n['id'] }).to include(
        request_1.id.to_s, request_2.id.to_s
      )

      post '/graphql', params: { query: my_completed_requests_query('REVIEW_RECEIVED') }, headers: get_auth_header_from(user)
      json = JSON.parse response.body
      expect(json['data']['myCompletedRequestsConnection']['totalCount']).to eq 2
      expect(json['data']['myCompletedRequestsConnection']['nodes'].collect { |n| n['id'] }).to include(
        request_4.id.to_s, request_5.id.to_s
      )
    end
  end
end
