# frozen_string_literal: true

require 'rails_helper'

def allergens_query
  <<~GQL
    query{
      allergens {
        id
        name
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    describe '#allergens' do
      it('returns allergens') do
        create(:allergen, name: 'foo')
        post '/graphql', params: { query: allergens_query }
        json = JSON.parse(response.body)
        data = json['data']['allergens']
        allergen = data.find { |d| d['name'] == 'foo' }
        expect(allergen).to be_truthy
        expect(allergen['id'].present?).to be true
      end
    end
  end
end
