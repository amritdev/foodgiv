# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def user_review_connection(user_id:, type: nil)
  <<~GQL
    query {
      userReviewsConnection(filter: {
        userId: #{user_id},
        #{
          if type.nil?
            ''
          else
            "type: #{type}"
          end
        }
      }) {
        totalCount
        nodes {
          id
        }
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    let(:user) { create(:user) }

    it 'returns reviews that given user received' do
      review_1 = create(:review, giver: user, reviewer: user)
      review_2 = create(:review, :for_giver, giver: user)
      review_3 = create(:review, receiver: user, reviewer: user)
      review_4 = create(:review, :for_receiver, receiver: user)
      review_5 = create(:review)
      post '/graphql', params: { query: user_review_connection(user_id: user.id) }, headers: get_auth_header_from(create(:user))
      json = JSON.parse response.body
      expect(json['data']['userReviewsConnection']['totalCount']).to eq 2
      expect(json['data']['userReviewsConnection']['nodes'].collect { |n| n['id'] }).to include(
        review_2.id.to_s, review_4.id.to_s
      )

      post '/graphql', params: { query: user_review_connection(user_id: user.id, type: 'FOR_GIVER') }, headers: get_auth_header_from(create(:user))
      json = JSON.parse response.body
      expect(json['data']['userReviewsConnection']['totalCount']).to eq 1
      expect(json['data']['userReviewsConnection']['nodes'].collect { |n| n['id'] }).to include(
        review_2.id.to_s
      )

      post '/graphql', params: { query: user_review_connection(user_id: user.id, type: 'FOR_RECEIVER') }, headers: get_auth_header_from(create(:user))
      json = JSON.parse response.body
      expect(json['data']['userReviewsConnection']['totalCount']).to eq 1
      expect(json['data']['userReviewsConnection']['nodes'].collect { |n| n['id'] }).to include(
        review_4.id.to_s
      )
    end
  end
end
