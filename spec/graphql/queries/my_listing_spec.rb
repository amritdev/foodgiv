# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

def build_my_lising_query_param(param)
  return '' unless param && param[:filter]

  <<~PARAM
    (
      filter: {
        status: #{param[:filter][:status]}
      }
    )
  PARAM
end

def my_list_query(input = { param: nil })
  <<~GQL
    query {
      myListingConnection #{build_my_lising_query_param(input[:param])} {
        pageInfo {
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
        }
        totalCount
        nodes {
          id
          title
          description
          user {
            id
            location {
              lat
              lng
            }
            firstName
            lastName
          }
          location {
            placeId
            lat
            lng
            formattedAddress
          }
          images {
            image {
              id
              url
            }
            large {
              url
            }
          }
          pickupTime
          expDate
          labels {
            name
          }
          allergens {
            name
          }
          requestCount
          requests {
            totalCount
          }
        }
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    let(:user) { create(:user) }
    let(:food_listed) { create(:food, :listed, user: user) }
    let(:food_unlisted) { create(:food, :unlisted, user: user) }
    let(:food_accepted) { create(:food, :accepted, user: user) }
    let(:food_completed) { create(:food, :completed, user: user) }
    let(:food_expired) { create(:food, :expired, user: user) }
    let(:message) { create(:food, food: food) }
    let(:giver_message) { create(:message, :giver_message, food: food) }
    let(:receiver_message) { create(:message, :receiver_message, food: food) }

    describe '#my_list_query' do
      it 'generates myFoodListingConnection query' do
        expect(my_list_query).to be_present
        expect(my_list_query(param: { filter: { status: 'LISTED' } })).to be_present
      end
    end

    describe '#myListingConnection' do
      it 'returns foods of given user' do
        food_listed
        food_unlisted
        food_accepted
        food_completed
        post '/graphql', params: { query: my_list_query }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        data = json['data']['myListingConnection']
        expect(data['totalCount']).to eq 4
      end

      it 'can filter by status' do
        food_listed
        food_unlisted
        food_accepted
        food_completed
        food_expired
        post '/graphql', params: { query: my_list_query(param: { filter: { status: 'LISTED' } }) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        data = json['data']['myListingConnection']
        expect(data['totalCount']).to eq 2
        expect(data['nodes'][0]['id']).to eq food_expired.id.to_s
        post '/graphql', params: { query: my_list_query(param: { filter: { status: 'UNLISTED' } }) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        data = json['data']['myListingConnection']
        expect(data['totalCount']).to eq 1
        expect(data['nodes'][0]['id']).to eq food_unlisted.id.to_s
        post '/graphql', params: { query: my_list_query(param: { filter: { status: 'ACCEPTED' } }) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        data = json['data']['myListingConnection']
        expect(data['totalCount']).to eq 1
        expect(data['nodes'][0]['id']).to eq food_accepted.id.to_s
        post '/graphql', params: { query: my_list_query(param: { filter: { status: 'ARCHIVED' } }) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        data = json['data']['myListingConnection']
        expect(data['totalCount']).to eq 2
        post '/graphql', params: { query: my_list_query(param: { filter: { status: 'REQUESTED' } }) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        data = json['data']['myListingConnection']
        expect(data['totalCount']).to eq 2
      end
    end
  end
end
