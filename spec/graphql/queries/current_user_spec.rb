# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def current_user_query(input)
  <<~GQL
    query {
      currentUser(
        token: "#{input[:token]}"
      ){
        id
        location {
          id
        }
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    describe '#current_user' do
      it('returns current user') do
        user = create(:user)
        token = JsonWebToken.encode(user.id)
        post '/graphql', params: { query: current_user_query({ token: token }) }, headers: build_mock_auth_header(token)
        json = JSON.parse(response.body)
        user_data = json['data']['currentUser']
        expect(user_data['id']).to eq(user.id.to_s)
      end

      it('includes location field') do
        user = create(:user)
        user.save!
        token = JsonWebToken.encode(user.id)
        post '/graphql', params: { query: current_user_query({ token: token }) }, headers: build_mock_auth_header(token)
        json = JSON.parse(response.body)
        user_data = json['data']['currentUser']
        expect(user_data['location']['id']).to be_present
      end
    end
  end
end
