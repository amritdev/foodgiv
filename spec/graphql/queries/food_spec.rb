# frozen_string_literal: true

def food_query(id)
  <<~GQL
    query {
      food(id: #{id}) {
        id
        title
        description
        user {
          id
          location {
            lat
            lng
          }
          firstName
          lastName
        }
        location {
          placeId
          lat
          lng
          formattedAddress
        }
        images {
          image {
            id
            url
          }
          large {
            url
          }
        }
        pickupTime
        expDate
        labels {
          name
        }
        allergens {
          name
        }
        requestCount
      }
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    let(:user) { create(:user) }
    let(:food) { create(:food) }

    describe '#food' do
      it 'returns food by id' do
        post '/graphql', params: { query: food_query(food.id) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        food_data = json['data']['food']
        expect(food_data).to include(
          'id' => food.id.to_s,
          'title' => food.title
        )
      end
    end
  end
end
