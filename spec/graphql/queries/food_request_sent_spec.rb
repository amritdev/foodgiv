# frozen_string_literal: true

def food_request_sent_query(id)
  <<~GQL
    query {
      foodRequestSent(id: #{id})
    }
  GQL
end

module Types
  RSpec.describe QueryType, type: :request do
    let(:user) { create(:user) }
    let(:food) { create(:food) }
    let(:request) { create(:request) }

    describe '#food_request_sent' do
      it 'returns false when request is not sent' do
        post '/graphql', params: { query: food_request_sent_query(food.id) }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        expect(json['data']['foodRequestSent']).to eq false
      end

      it 'returns true when request is sent' do
        post '/graphql', params: { query: food_request_sent_query(request.food.id) }, headers: get_auth_header_from(request.receiver)
        json = JSON.parse response.body
        expect(json['data']['foodRequestSent']).to eq true
      end
    end
  end
end
