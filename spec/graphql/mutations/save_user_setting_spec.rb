# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def save_user_setting_mutation(setting)
  <<~GQL
    mutation {
      saveUserSetting(
        input: {
          setting: {
            firstName: "#{setting[:first_name]}"
            #{setting[:last_name].nil? ? '' : "lastName: \"#{setting[:last_name]}\""}
            email: "#{setting[:email]}"
            #{setting[:phone].nil? ? '' : "phone: \"#{setting[:phone]}\""}
            #{setting[:password].nil? ? '' : "password: \"#{setting[:password]}\""}
            #{setting[:unlink_google].nil? ? '' : "unlinkGoogle : #{setting[:unlink_google]}"}
            #{setting[:unlink_facebook].nil? ? '' : "unlinkFacebook : #{setting[:unlink_facebook]}"}
            #{setting[:notification_allowed].nil? ? '' : "notificationAllowed: #{setting[:notification_allowed]}"}
            #{setting[:notification_frequency].nil? ? '' : "notificationFrequency: #{setting[:notification_frequency]}"}
            #{setting[:notification_distance].nil? ? '' : "notificationDistance: #{setting[:notification_distance]}"}
            #{if setting[:location].nil?
                ''
              else
                "
                location: {
                  #{setting[:location][:id].nil? ? '' : "id: #{setting[:location][:id]}"}
                  placeId: \"#{setting[:location][:place_id]}\"
                  lat: #{setting[:location][:lat]}
                  lng: #{setting[:location][:lng]}
                  formattedAddress: \"#{setting[:location][:formatted_address]}\"
                }
                "
              end}
            #{if setting[:device_info].nil?
                ''
              else
                "
                deviceInfo: {
                  deviceToken: \"#{setting[:device_info][:device_token]}\"
                  deviceType: #{setting[:device_info][:device_type]}
                }
                "
              end}
          }
        }
      ) {
        user {
          id
          firstName
          lastName
          email
          location {
            id
            placeId
            lat
            lng
            formattedAddress
          }
          phone
          phoneVerified
          googleUserId
          facebookUserId
          deviceInfo {
            deviceToken
            deviceType
          }
          notificationAllowed
          notificationDistance
          notificationFrequency
        }
      }
    }
  GQL
end

def sign_in_by_email_mutation(email, password)
  <<~GQL
    mutation {
      signinUser(
        input: {
          credentials: {
            provider: EMAIL
            email: "#{email}"
            password: "#{password}"
          }
        }
      ) {
        user {
          id
        }
        token
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#save_user_setting' do
      context 'when jwt token is missing or invalid' do
        it 'throws authentication exception' do
          setting = {
            first_name: 'foo',
            email: 'foo@bar.com',
            password: '',
            unlink_google: false
          }
          post '/graphql', params: { query: save_user_setting_mutation(setting) }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse(response.body)
          expect(json['data']['saveUserSetting']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq('AUTHENTICATION_FAILED')
        end
      end

      context 'when jwt token is valid' do
        context 'when valid information is given' do
          it 'updates user info' do
            user = create(:user)
            setting = {
              first_name: 'brave',
              last_name: 'master',
              email: 'bravemaster619@hotmail.com',
              phone: '1234567890',
              unlink_google: true,
              unlink_facebook: true,
              notification_allowed: true,
              notification_frequency: 'TWICE_PER_DAY',
              notification_distance: 15,
              location: {
                id: user.location.id,
                place_id: 'ChIJOwg_06VPwokRYv534QaPC8g',
                lat: 40.7128,
                lng: -74.0060,
                formatted_address: 'New York, USA'
              }
            }
            post '/graphql', params: { query: save_user_setting_mutation(setting) }, headers: get_auth_header_from(user)
            json = JSON.parse(response.body)
            user = json['data']['saveUserSetting']['user']
            expect(user['firstName']).to eq(setting[:first_name])
            expect(user['lastName']).to eq(setting[:last_name])
            expect(user['email']).to eq(setting[:email])
            expect(user['phone']).to eq(setting[:phone])
            expect(user['googleUserId']).to be_nil
            expect(user['facebookUserId']).to be_nil
            expect(user['notificationAllowed']).to eq(setting[:notification_allowed])
            expect(user['notificationFrequency']).to eq(setting[:notification_frequency])
            expect(user['notificationDistance']).to eq(setting[:notification_distance])
            expect(user['location']['id']).to eq(setting[:location][:id].to_s)
            expect(user['location']['placeId']).to eq(setting[:location][:place_id])
            expect(user['location']['lat']).to be_within(0.000001).of(setting[:location][:lat])
            expect(user['location']['lng']).to be_within(0.000001).of(setting[:location][:lng])
            expect(user['location']['formattedAddress']).to eq(setting[:location][:formatted_address])
          end

          it 'is able to change password' do
            user = create(:user)
            password = '1234567890'
            setting = {
              first_name: user.first_name,
              last_name: user.last_name,
              email: user.email,
              password: password
            }
            post '/graphql', params: { query: save_user_setting_mutation(setting) }, headers: get_auth_header_from(user)
            post '/graphql', params: { query: sign_in_by_email_mutation(user.email, password) }
            json = JSON.parse(response.body)
            expect(json['data']['signinUser']['user']['id']).to eq(user.id.to_s)
            expect(json['data']['signinUser']['token']).to be_present
          end

          it 'creates a location if user is without one' do
            user = create(:user, :no_location)
            setting = {
              email: user.email,
              first_name: user.first_name,
              last_name: user.last_name,
              phone: user.phone,
              location: build(:location)
            }
            post '/graphql', params: { query: save_user_setting_mutation(setting) }, headers: get_auth_header_from(user)
            json = JSON.parse(response.body)
            user = json['data']['saveUserSetting']['user']
            expect(user['location']['id']).to be_present
          end

          it 'is able to save device info' do
            user = create(:user)
            setting = {
              device_info: {
                device_token: 'dummy token',
                device_type: 'ANDROID'
              }
            }
            post '/graphql', params: { query: save_user_setting_mutation(setting) }, headers: get_auth_header_from(user)
            json = JSON.parse(response.body)
            expect(json['data']['saveUserSetting']['user']['deviceInfo']).to include(
              'deviceToken' => 'dummy token',
              'deviceType' => 'ANDROID'
            )
          end

          context 'when phone number is changed' do
            it 'set phone verified false' do
              user = create(:user, phone: '1234567890')
              setting = {
                email: user.email,
                first_name: user.first_name,
                last_name: user.last_name,
                phone: '98765433210'
              }
              post '/graphql', params: { query: save_user_setting_mutation(setting) }, headers: get_auth_header_from(user)
              json = JSON.parse response.body
              user = json['data']['saveUserSetting']['user']
              expect(user['phoneVerified']).to eq false
            end
          end
        end

        context 'when invalid information is given' do
          it 'throws error if email is duplicated' do
            user_1 = create(:user)
            user_2 = create(:user)
            setting = {
              email: user_1.email,
              first_name: user_2.first_name,
              last_name: user_2.last_name,
              phone: user_2.phone
            }
            post '/graphql', params: { query: save_user_setting_mutation(setting) }, headers: get_auth_header_from(user_2)
            json = JSON.parse(response.body)
            expect(json['errors'][0]['extensions']['code']).to eq('email_duplicated')
          end
        end
      end
    end
  end
end
