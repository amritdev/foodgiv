# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'
require 'stripe_mock'
include SpecMainHelper

def create_subscription_mutation(payment_id)
  <<~GQL
    mutation {
      createSubscription(
        input: {
          paymentId: "#{payment_id}"
        }
      ) {
        user {
          stripeCustomerId
          stripeSubscriptionId
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    let(:stripe_helper) { StripeMock.create_test_helper }
    let(:payment_method) do
      Stripe::PaymentMethod.create({
                                     type: 'card',
                                     card: card_info
                                   })
    end
    let(:user) { create(:user) }
    let(:card_info) { { number: '4242424242424242', exp_month: '01', exp_year: '2050', cvc: '123' } }

    before do
      StripeMock.start
    end

    after { StripeMock.stop }

    describe '#create_subscription' do
      it 'creates stripe customer and subscription' do
        # post '/graphql', params: { query: create_subscription_mutation(payment_method[:id]) }, headers: get_auth_header_from(user)
        # json = JSON.parse response.body
        # puts json
        puts 'WARNING: Types::MutationType#create_subscription spec is not implemented yet'
      end
    end
  end
end
