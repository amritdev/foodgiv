# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'
require_relative './send_verification_code_spec.rb' unless defined? send_verification_code_mutation
include SpecMainHelper

def verify_phone_mutation(code)
  <<~GQL
    mutation {
      verifyPhone(
        input: {
          code: "#{code}"
        }
      ) {
        success
        message
        code
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#verify_phone' do
      context 'when jwt token is invalid' do
        it 'throws authentication exception' do
          post '/graphql', params: { query: verify_phone_mutation('foo') }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse(response.body)
          expect(json['data']['verifyPhone']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'AUTHENTICATION_FAILED'
        end
      end

      context 'when user is already verified' do
        it 'returns code phone_already_verified' do
          user = create(:user)
          post '/graphql', params: { query: verify_phone_mutation('foo') }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['data']['verifyPhone']['success']).to eq false
          expect(json['data']['verifyPhone']['code']).to eq 'phone_already_verified'
        end
      end

      context 'when user verification code is not set' do
        it 'returns code verification_code_empty' do
          user = create(:user, phone_verified: false)
          post '/graphql', params: { query: verify_phone_mutation('foo') }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['data']['verifyPhone']['success']).to eq false
          expect(json['data']['verifyPhone']['code']).to eq 'verification_code_empty'
        end
      end

      context 'when verification code does not match' do
        it 'returns code verification_code_mismatch' do
          user = create(:user, phone_verified: false)
          post '/graphql', params: { query: send_verification_code_mutation(user.phone) }, headers: get_auth_header_from(user)
          user = User.find user.id
          verification_code = user.verification_code
          post '/graphql', params: { query: verify_phone_mutation('code_that_never_works') }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['data']['verifyPhone']['success']).to eq false
          expect(json['data']['verifyPhone']['code']).to eq 'verification_code_mismatch'
        end
      end

      context 'when everything is ok' do
        it 'works fine' do
          user = create(:user, phone_verified: false)
          post '/graphql', params: { query: send_verification_code_mutation(user.phone) }, headers: get_auth_header_from(user)
          user = User.find user.id
          verification_code = user.verification_code
          post '/graphql', params: { query: verify_phone_mutation(verification_code) }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['data']['verifyPhone']['success']).to eq true
        end
      end
    end
  end
end
