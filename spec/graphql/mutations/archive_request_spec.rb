# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def archive_request_mutation(request_id)
  <<~GQL
    mutation {
      archiveRequest(
        input: {
          requestId: #{request_id}
        }
      ) {
        request {
          giverArchived
          receiverArchived
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#archive_request' do
      context 'when user does not have relationship with request' do
        it 'returns code no_such_request' do
          user = create(:user)
          request = create(:request)
          post '/graphql', params: { query: archive_request_mutation(request.id) }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['errors'][0]['extensions']['code']).to eq 'no_such_request'
        end
      end

      context 'when user is giver' do
        it 'sets giver_archive true' do
          user = create(:user)
          request = create(:request, giver: user)
          post '/graphql', params: { query: archive_request_mutation(request.id) }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['data']['archiveRequest']['request']).to include(
            'giverArchived' => true,
            'receiverArchived' => false
          )
        end
      end

      context 'when user is receiver' do
        it 'sets receiver_archived true' do
          user = create(:user)
          request = create(:request, receiver: user)
          post '/graphql', params: { query: archive_request_mutation(request.id) }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['data']['archiveRequest']['request']).to include(
            'giverArchived' => false,
            'receiverArchived' => true
          )
        end
      end
    end
  end
end
