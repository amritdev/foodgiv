# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def send_otp_mutation(email)
  <<~GQL
    mutation {
      sendOtp(
        input: {
          email: "#{email}"
        }
      ) {
        success
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#send_otp' do
      let(:user) { create(:user) }

      it 'returns boolean' do
        post '/graphql', { params: { query: send_otp_mutation(user.email) } }
        json = JSON.parse response.body
        expect(json['data']['sendOtp']).to include(
          'success' => be_in([true, false])
        )
      end
    end
  end
end
