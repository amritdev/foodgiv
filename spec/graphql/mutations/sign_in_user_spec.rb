# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'
require_relative './send_otp_spec.rb' unless defined? send_otp_mutation
include SpecMainHelper

def sign_in_user_mutation(credentials)
  <<~GQL
    mutation {
      signinUser(
        input: {
          credentials: {
            provider: #{credentials[:provider]}
            #{if credentials[:email].present?
                "
              email: \"#{credentials[:email]}\"
            "
              else
                ''
            end}
            #{if credentials[:access_token].present?
                "
              accessToken: \"#{credentials[:access_token]}\"
            "
              else
                ''
            end}
            #{if credentials[:password].present?
                "
              password: \"#{credentials[:password]}\"
            "
              else
                ''
            end}
            #{if credentials[:otp].present?
                "
              otp: \"#{credentials[:otp]}\"
            "
              else
                ''
            end}
          }
        }
      ) {
        user {
          id
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    let(:user) { create(:user) }

    describe '#sign_in_user' do
      it 'sign in user by otp code' do
        post '/graphql', { params: { query: send_otp_mutation(user.email) } }
        otp = Rails.cache.read("users:#{user.id}:otp")
        # with wrong password
        post '/graphql', { params: { query: sign_in_user_mutation({ provider: 'EMAIL', email: user.email, otp: "#{otp}wrong" }) } }
        json = JSON.parse response.body
        expect(json['data']['signinUser']['user']).to be_nil
        # with correct password
        post '/graphql', { params: { query: sign_in_user_mutation({ provider: 'EMAIL', email: user.email, otp: otp }) } }
        json = JSON.parse response.body
        expect(json['data']['signinUser']['user']).to include(
          'id' => user.id.to_s
        )
        # double attempt with the same password
        post '/graphql', { params: { query: sign_in_user_mutation({ provider: 'EMAIL', email: user.email, otp: otp }) } }
        json = JSON.parse response.body
        expect(json['data']['signinUser']['user']).to be_nil
      end
    end
  end
end
