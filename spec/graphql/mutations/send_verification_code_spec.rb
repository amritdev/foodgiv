# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def send_verification_code_mutation(phone)
  <<~GQL
    mutation {
      sendVerificationCode(input: {
        phone: "#{phone}"
      }) {
        success
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#send_verification_code' do
      context 'when jwt token is invalid' do
        it 'throws authentication exception' do
          post '/graphql', params: { query: send_verification_code_mutation('1111111') }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse(response.body)
          expect(json['data']['sendVerificationCode']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq('AUTHENTICATION_FAILED')
        end
      end

      context 'when user phone number is valid' do
        it 'changes verification code' do
          user = create(:user)
          old_code = user.verification_code
          post '/graphql', params: { query: send_verification_code_mutation(user.phone) }, headers: get_auth_header_from(user)
          user = User.find user.id
          expect(user.verification_code).to be_present
          expect(user.verification_code).not_to eq(old_code)
        end
      end

      it 'controls throttling' do
        user = create(:user)
        post '/graphql', params: { query: send_verification_code_mutation(user.phone) }, headers: get_auth_header_from(user)
        post '/graphql', params: { query: send_verification_code_mutation(user.phone) }, headers: get_auth_header_from(user)
        json = JSON.parse(response.body)
        expect(json['errors'][0]['extensions']['code']).to eq('otp_too_many_requests')
      end
    end
  end
end
