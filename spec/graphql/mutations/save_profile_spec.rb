# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def save_profile_mutation(input)
  <<~GQL
    mutation {
      saveProfile(
        input: {
          profile: {
            summary: "#{input[:profile][:summary]}"
            favoriteFood: "#{input[:profile][:favorite_food]}"
            hatedFood: "#{input[:profile][:hated_food]}"
            #{
              if input[:profile][:image_id].nil?
                ''
              else
                "
                imageId: #{input[:profile][:image_id]}
                "
              end
            }
            #{
              if input[:profile][:location].nil?
                ''
              else
                "
                location: {
                  lat: #{input[:profile][:location][:lat]}
                  lng: #{input[:profile][:location][:lng]}
                  formattedAddress: \"#{input[:profile][:location][:formatted_address]}\"
                  placeId: \"#{input[:profile][:location][:place_id]}\"
                }
                "
              end
            }
          }
        }
      ) {
        user {
          summary
          favoriteFood
          hatedFood
          profileImage {
            id
            image {
              id
            }
            small {
              url
            }
          }
          location {
            placeId
            lat
            lng
            formattedAddress
          }
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#save_profile' do
      it('saves profile') do
        user = create(:user)
        location = build(:location)
        image = create(:image)
        token = JsonWebToken.encode(user.id)
        profile = {
          summary: Faker::Lorem.paragraph(sentence_count: 3),
          favorite_food: Faker::Lorem.words(number: Faker::Number.between(from: 0, to: 10)).join(','),
          hated_food: Faker::Lorem.words(number: Faker::Number.between(from: 0, to: 10)).join(','),
          image_id: image.id,
          location: location
        }
        post '/graphql', params: { query: save_profile_mutation({ profile: profile }) }, headers: build_mock_auth_header(token)
        json = JSON.parse(response.body)
        user_data = json['data']['saveProfile']['user']
        expect(user_data['summary']).to eq(profile[:summary])
        expect(user_data['favoriteFood']).to eq(profile[:favorite_food])
        expect(user_data['hatedFood']).to eq(profile[:hated_food])
        expect(user_data['profileImage']['id']).to eq(image.id.to_s)
        expect(user_data['profileImage']['small']['url']).to be_present
        expect(user_data['location']['lat']).to be_within(0.000001).of(location.lat)
      end

      it('throws error if authentication is failed') do
        user = create(:user)
        old_summary = user[:summary]
        profile = {
          summary: 'this is one sentence so it can not be accidentally same to fake summary',
          favorite_food: Faker::Lorem.words(number: Faker::Number.between(from: 0, to: 10)).join(','),
          hated_food: Faker::Lorem.words(number: Faker::Number.between(from: 0, to: 10)).join(',')
        }
        post '/graphql', params: { query: save_profile_mutation({ profile: profile }) }, headers: build_mock_auth_header('1234')
        expect(user[:summary]).not_to eq(profile[:summary])
        expect(user[:summary]).to eq(old_summary)
        json = JSON.parse(response.body)
        expect(json['errors'][0]['extensions']['code'] == 'AUTHENTICATION_FAILED')
      end
    end
  end
end
