# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def archive_message_mutation(message_id)
  <<~GQL
    mutation {
      archiveMessage(
        input: {
          messageId: #{message_id}
        }
      ) {
        success
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#archive_message' do
      context 'when jwt token is invalid' do
        it 'throws authentication exception' do
          post '/graphql', params: { query: archive_message_mutation(1) }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse response.body
          expect(json['data']['archiveMessage']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'AUTHENTICATION_FAILED'
        end
      end

      context 'when message is not found' do
        it 'returns false' do
          post '/graphql', params: { query: archive_message_mutation(1) }, headers: get_auth_header_from(create(:user))
          json = JSON.parse response.body
          expect(json['data']['archiveMessage']['success']).to eq false
        end
      end

      context 'when a user is a sender' do
        it 'archives message for sender' do
          message = create(:message)
          post '/graphql', params: { query: archive_message_mutation(message.id) }, headers: get_auth_header_from(message.sender)
          message.reload
          expect(message.sender_archived).to eq true
        end
      end

      context 'when a user is a receiver' do
        it 'archives message for receiver' do
          message = create(:message)
          post '/graphql', params: { query: archive_message_mutation(message.id) }, headers: get_auth_header_from(message.receiver)
          message.reload
          expect(message.receiver_archived).to eq true
        end
      end
    end
  end
end
