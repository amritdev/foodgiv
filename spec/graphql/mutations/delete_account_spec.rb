# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def delete_account_mutation
  <<~GQL
    mutation {
      deleteAccount(input: {}) {
        success
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#delete_account' do
      it 'deletes an account' do
        user = create(:user)
        post '/graphql', params: { query: delete_account_mutation }, headers: get_auth_header_from(user)
        json = JSON.parse response.body
        expect(json['data']['deleteAccount']['success']).to eq true
        user.reload
        expect(user.status).to eq 'DELETED'
      end
    end
  end
end
