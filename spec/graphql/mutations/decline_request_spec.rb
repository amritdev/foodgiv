# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def decline_request_mutation(request_id)
  <<~GQL
    mutation {
      declineRequest (
        input: {
          requestId: #{request_id}
        }
      ) 
      {
        request {
          id
          message
          giver {
            id
          }
          receiver {
            id
          }
          food {
            id
          }
          acceptedAt
          declinedAt
          cancelledAt
          completedAt
          read
          status
          createdAt
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#decline_request' do
      context 'when jwt token is invalid' do
        it 'throws authentication exception' do
          post '/graphql', params: { query: decline_request_mutation(1) }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse response.body
          expect(json['data']['declineRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'AUTHENTICATION_FAILED'
        end
      end

      context 'when request is not found' do
        it 'returns code no_such_request' do
          post '/graphql', params: { query: decline_request_mutation(1) }, headers: get_auth_header_from(create(:user))
          json = JSON.parse response.body
          expect(json['data']['declineRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'no_such_request'
        end
      end

      context 'when request is already handled' do
        it 'returns already handled code' do
          # accepted request
          request = create(:request, :accepted)
          post '/graphql', params: { query: decline_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['declineRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'request_already_accepted'
          # declined request
          request = create(:request, :declined)
          post '/graphql', params: { query: decline_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['declineRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'request_already_declined'
          # cancelled request
          request = create(:request, :cancelled)
          post '/graphql', params: { query: decline_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['declineRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'request_already_cancelled'
        end
      end

      context 'when everything is ok' do
        it 'changes request status to be DECLINED' do
          request = create(:request)
          post '/graphql', params: { query: decline_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          request = json['data']['declineRequest']['request']
          expect(request['declinedAt']).to be_present
          expect(request['status']).to eq 'DECLINED'
        end
      end
    end
  end
end
