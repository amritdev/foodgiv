# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def save_review_mutations(review)
  <<~GQL
    mutation {
      saveReview(
        input: {
          review: {
            requestId: #{review[:request_id]}
            #{if review[:collector_exchange_exp].nil?
                ''
              else
                "
                collectorExchangeExp: #{review[:collector_exchange_exp]}
                "
            end }
            #{if review[:food_quality].nil?
                ''
              else
                "
                foodQuality: #{review[:food_quality]}
                "
            end}
            #{if review[:pickup_exp].nil?
                ''
              else
                "
                pickupExp: #{review[:pickup_exp]}
                "
            end}
            #{if review[:feedback].nil?
                ''
              else
                "
                feedback: \"#{review[:feedback]}\"
                "
            end}
          }
        }
      ) {
        review {  
          id
          request {
            id
          }
          giver {
            id
          }
          receiver {
            id
          }
          reviewer {
            id
          }
          food {
            id
          }
          pickupExp
          foodQuality
          collectorExchangeExp
          feedback
          createdAt
          updatedAt
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#save_review' do
      context 'when jwt token is invalid' do
        it 'throws authentication exception' do
          post '/graphql', params: { query: save_review_mutations({ request_id: 1 }) }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse response.body
          expect(json['data']['saveReview']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'AUTHENTICATION_FAILED'
        end
      end

      context 'when request is not found' do
        it 'returns code no_such_request' do
          post '/graphql', params: { query: save_review_mutations({ request_id: 1 }) }, headers: get_auth_header_from(create(:user))
          json = JSON.parse response.body
          expect(json['data']['saveReview']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'no_such_request'
        end
      end

      context 'when request is not completed' do
        it 'returns code request_not_completed' do
          request = create(:request)
          post '/graphql', params: { query: save_review_mutations({ request_id: request.id }) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['saveReview']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'request_not_completed'
        end
      end

      context 'when user is not related to request' do
        it 'returns code no_such_request' do
          request = create(:request, :completed)
          post '/graphql', params: { query: save_review_mutations({ request_id: request.id }) }, headers: get_auth_header_from(create(:user))
          json = JSON.parse response.body
          expect(json['data']['saveReview']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'no_such_request'
        end
      end

      context 'when rating detail is invalid' do
        it 'returns code invalid_rating' do
          request = create(:request, :completed)
          post '/graphql', params: { query: save_review_mutations({ request_id: request.id }) }, headers: get_auth_header_from(request.receiver)
          json = JSON.parse response.body
          expect(json['data']['saveReview']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'invalid_rating'
          # missing pickup_exp in receiver's review
          request = create(:request, :completed)
          post '/graphql', params: { query: save_review_mutations({ request_id: request.id, food_quality: 0 }) }, headers: get_auth_header_from(request.receiver)
          json = JSON.parse response.body
          expect(json['data']['saveReview']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'invalid_rating'
          # missing food_quality in receiver's review
          request = create(:request, :completed)
          post '/graphql', params: { query: save_review_mutations({ request_id: request.id, pickup_exp: 1 }) }, headers: get_auth_header_from(request.receiver)
          json = JSON.parse response.body
          expect(json['data']['saveReview']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'invalid_rating'
          # invalid pickup_exp in receiver's review
          request = create(:request, :completed)
          post '/graphql', params: { query: save_review_mutations({ request_id: request.id, pickup_exp: -1, food_quality: 4.1 }) }, headers: get_auth_header_from(request.receiver)
          json = JSON.parse response.body
          expect(json['data']['saveReview']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'invalid_rating'
          # missing collector_exchange_exp in giver's review
          request = create(:request, :completed)
          post '/graphql', params: { query: save_review_mutations({ request_id: request.id, pickup_exp: 1, food_quality: 4 }) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['saveReview']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'invalid_rating'
        end
      end

      context 'when everything is ok' do
        context 'when reviewer is a food giver' do
          it 'creates a review for receiver' do
            request = create(:request, :completed)
            post '/graphql', params: { query: save_review_mutations({ request_id: request.id, collector_exchange_exp: 4, feedback: 'feedback' }) }, headers: get_auth_header_from(request.giver)
            json = JSON.parse response.body
            review = json['data']['saveReview']['review']
            expect(review['id']).to be_present
            expect(review['giver']['id']).to eq request.giver.id.to_s
            expect(review['receiver']['id']).to eq request.receiver.id.to_s
            expect(review['request']['id']).to eq request.id.to_s
            expect(review['food']['id']).to eq request.food.id.to_s
            expect(review['reviewer']['id']).to eq request.giver.id.to_s
            expect(review['collectorExchangeExp']).to eq 4
            expect(review['foodQuality']).to be_nil
            expect(review['pickupExp']).to be_nil
            expect(review['feedback']).to eq 'feedback'
          end
        end

        context 'when reviewer is a food receiver' do
          it 'creates a review for giver' do
            request = create(:request, :completed)
            post '/graphql', params: { query: save_review_mutations({ request_id: request.id, food_quality: 5, pickup_exp: 3, feedback: 'feedback' }) }, headers: get_auth_header_from(request.receiver)
            json = JSON.parse response.body
            review = json['data']['saveReview']['review']
            expect(review['id']).to be_present
            expect(review['giver']['id']).to eq request.giver.id.to_s
            expect(review['receiver']['id']).to eq request.receiver.id.to_s
            expect(review['request']['id']).to eq request.id.to_s
            expect(review['food']['id']).to eq request.food.id.to_s
            expect(review['reviewer']['id']).to eq request.receiver.id.to_s
            expect(review['collectorExchangeExp']).to be_nil
            expect(review['foodQuality']).to eq 5
            expect(review['pickupExp']).to eq 3
            expect(review['feedback']).to eq 'feedback'
          end
        end

        it 'updates review stats of the reviewer' do
          request = create(:request, :completed)
          post '/graphql', params: { query: save_review_mutations({ request_id: request.id, food_quality: 5, pickup_exp: 3, feedback: 'feedback' }) }, headers: get_auth_header_from(request.receiver)
          giver = User.find request.giver.id
          expect(giver.giver_review_count).to eq 1
          expect(giver.giver_rating).to eq 4
          post '/graphql', params: { query: save_review_mutations({ request_id: request.id, food_quality: 5, pickup_exp: 5, feedback: 'feedback' }) }, headers: get_auth_header_from(request.receiver)
          giver = User.find request.giver.id
          expect(giver.giver_review_count).to eq 1
          expect(giver.giver_rating).to eq 5
        end
      end
    end
  end
end
