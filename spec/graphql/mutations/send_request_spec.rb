# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def send_request_mutation(food_id, message = nil)
  <<~GQL
    mutation {
      sendRequest(
        input: {
          foodId: #{food_id}
          #{message.nil? ? '' : "message: \"#{message}\""}
        }
      ){
        request {
          id
          message
          giver {
            id
          }
          receiver {
            id
          }
          food {
            id
          }
          acceptedAt
          declinedAt
          cancelledAt
          read
          status
          createdAt
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#send_request' do
      context 'when jwt token is invalid' do
        it 'throws authentication exception' do
          post '/graphql', params: { query: send_request_mutation(1, 'this_wont_work') }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse response.body
          expect(json['data']['sendRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'AUTHENTICATION_FAILED'
        end
      end

      context 'when user is not subscribed' do
        it 'returns code user_not_paid' do
          user = create(:user, stripe_customer_id: nil, stripe_subscription_id: nil)
          food = create(:food)
          post '/graphql', params: { query: send_request_mutation(food.id, 'this_wont_work') }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['data']['sendRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'user_not_paid'
        end
      end

      context 'when food does not exist' do
        it 'returns code no_such_food' do
          user = create(:user, :subscribed)
          post '/graphql', params: { query: send_request_mutation(1, 'this_wont_work') }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['data']['sendRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'no_such_food'
          # soft deleted
          food = create(:food, :deleted)
          post '/graphql', params: { query: send_request_mutation(food.id, 'this_wont_work') }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['data']['sendRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'no_such_food'
        end
      end

      context 'when food is unlisted/completed/failed' do
        it 'returns code food_not_listed' do
          user = create(:user, :subscribed)
          food_unlisted = create(:food, :unlisted)
          food_completed = create(:food, :completed)
          food_failed = create(:food, :failed)
          # unlisted
          post '/graphql', params: { query: send_request_mutation(food_unlisted.id) }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['errors'][0]['extensions']['code']).to eq 'food_not_listed'
          # completed
          post '/graphql', params: { query: send_request_mutation(food_completed.id) }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['errors'][0]['extensions']['code']).to eq 'food_not_listed'
          # failed
          post '/graphql', params: { query: send_request_mutation(food_failed.id) }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['errors'][0]['extensions']['code']).to eq 'food_not_listed'
        end
      end

      context 'when giver is receiver' do
        it 'returns code cannot_send_request_to_own_food' do
          user = create(:user, :subscribed)
          food = create(:food, user: user)
          post '/graphql', params: { query: send_request_mutation(food.id) }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          expect(json['errors'][0]['extensions']['code']).to eq 'cannot_send_request_to_own_food'
        end
      end

      context 'when request is already sent' do
        it 'returns code request_already_sent' do
          request = create(:request)
          post '/graphql', params: { query: send_request_mutation(request.food.id) }, headers: get_auth_header_from(request.receiver)
          json = JSON.parse response.body
          expect(json['errors'][0]['extensions']['code']).to eq 'request_already_sent'
        end
      end

      context 'when everything is ok' do
        it 'creates a request' do
          user = create(:user, :subscribed)
          food = create(:food)
          post '/graphql', params: { query: send_request_mutation(food.id, 'test_message') }, headers: get_auth_header_from(user)
          json = JSON.parse response.body
          request = json['data']['sendRequest']['request']
          expect(request['id']).to be_present
          expect(request['message']).to eq 'test_message'
          expect(request['giver']['id']).to eq(food.user.id.to_s)
          expect(request['receiver']['id']).to eq(user.id.to_s)
          expect(request['food']['id']).to eq(food.id.to_s)
        end
      end
    end
  end
end
