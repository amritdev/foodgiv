# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def accept_request_mutation(request_id)
  <<~GQL
    mutation {
      acceptRequest(
        input: {
          requestId: #{request_id}
        }
      ) 
      {
        request {
          id
          message
          giver {
            id
          }
          receiver {
            id
          }
          food {
            id
          }
          acceptedAt
          declinedAt
          cancelledAt
          completedAt
          read
          status
          createdAt
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#accept_request' do
      context 'when jwt token is invalid' do
        it 'throws authentication exception' do
          post '/graphql', params: { query: accept_request_mutation(1) }, headers: build_mock_auth_header('token_that_never_works')
          json = JSON.parse response.body
          expect(json['data']['acceptRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'AUTHENTICATION_FAILED'
        end
      end

      context 'when request is not found' do
        it 'returns code no_such_request' do
          post '/graphql', params: { query: accept_request_mutation(1) }, headers: get_auth_header_from(create(:user))
          json = JSON.parse response.body
          expect(json['data']['acceptRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'no_such_request'
        end
      end

      context 'when request is already handled' do
        it 'returns already handled code' do
          # accepted request
          request = create(:request, :accepted)
          post '/graphql', params: { query: accept_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['acceptRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'request_already_accepted'
          # declined request
          request = create(:request, :declined)
          post '/graphql', params: { query: accept_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['acceptRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'request_already_declined'
          # cancelled request
          request = create(:request, :cancelled)
          post '/graphql', params: { query: accept_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['acceptRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'request_already_cancelled'
          # completed request
          request = create(:request, :completed)
          post '/graphql', params: { query: accept_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          expect(json['data']['acceptRequest']).to be_nil
          expect(json['errors'][0]['extensions']['code']).to eq 'request_already_completed'
        end
      end

      context 'when everything is ok' do
        it 'changes request status to be ACCEPTED' do
          request = create(:request)
          post '/graphql', params: { query: accept_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
          json = JSON.parse response.body
          request = json['data']['acceptRequest']['request']
          expect(request['acceptedAt']).to be_present
          expect(request['status']).to eq 'ACCEPTED'
        end

        it 'changes food status to be ACCEPTED' do
          request = create(:request)
          post '/graphql', params: { query: accept_request_mutation(request.id) }, headers: get_auth_header_from(request.giver)
          expect(Food.find(request.food.id).status).to eq 'ACCEPTED'
        end

        it 'declines other requests' do
          food = create(:food)
          request_1 = create(:request, food: food)
          request_2 = create(:request, food: food)
          request_3 = create(:request, food: food)
          post '/graphql', params: { query: accept_request_mutation(request_1.id) }, headers: get_auth_header_from(request_1.giver)
          accepted = Request.where(food_id: food.id, status: 'ACCEPTED').first
          expect(accepted.id).to eq request_1.id
          declined = Request.where(food_id: food.id, status: 'DECLINED').all
          expect(declined.length).to eq 2
        end
      end
    end
  end
end
