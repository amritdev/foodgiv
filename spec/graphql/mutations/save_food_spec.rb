# frozen_string_literal: true

require 'rails_helper'
require 'spec_main_helper'

include SpecMainHelper

def save_food_mutation(input)
  food = input[:food]
  <<~GQL
    mutation {
      saveFood(
        input: {
          food: {
            #{food[:id] ? "id: #{food[:id]}" : ''}
            title: "#{food[:title]}"
            description: "#{food[:description]}"
            foodType: #{food[:food_type]}
            quantity: #{food[:quantity]}
            pickupTime: "#{food[:pickup_time]}"
            expDate: "#{food[:exp_date]}"
            location: {
              placeId: "#{food[:location][:place_id]}"
              lat: #{food[:location][:lat]}
              lng: #{food[:location][:lng]}
              formattedAddress: "#{food[:location][:formatted_address]}"
            }
            labelsIds: #{JSON.generate food[:labels_ids]}
            allergensIds: #{JSON.generate food[:allergens_ids]}
            imagesIds: #{JSON.generate food[:images_ids]}
            status: #{food[:status]}
          }
        }
      ){
        food {
          id
          title
          description
          foodType
          quantity
          pickupTime
          expDate
          labels {
            id
            name
          }
          allergens {
            id
            name
          }
          location {
            id
            placeId
            lat
            lng
            formattedAddress
          }
          images {
            image {
              id
              url
            }
            large {
              url
            }
          }
          user {
            id
          }
          listedAt
        }
      }
    }
  GQL
end

module Types
  RSpec.describe MutationType, type: :request do
    describe '#save_food' do
      context 'when id is missing' do
        context 'when valid input is given' do
          it 'creates a food' do
            label = create(:label)
            allergen = create(:allergen)
            location = build(:location)
            image = create(:image, :food)
            user = create(:user)
            food_attr = {
              title: 'foo',
              description: 'foo description',
              food_type: 'UNPREPARED',
              quantity: 2,
              pickup_time: 'anytime',
              location: location,
              exp_date: 10.days.from_now.strftime('%Y-%m-%d'),
              labels_ids: [label.id],
              allergens_ids: [allergen.id],
              images_ids: [image.id],
              status: 'UNLISTED'
            }
            post '/graphql', params: { query: save_food_mutation({ food: food_attr }) }, headers: get_auth_header_from(user)
            json = JSON.parse(response.body)
            food = json['data']['saveFood']['food']
            expect(food['id']).to be_present
            expect(food['title']).to eq(food_attr[:title])
            expect(food['description']).to eq(food_attr[:description])
            expect(food['foodType']).to eq(food_attr[:food_type])
            expect(food['quantity']).to eq(food_attr[:quantity])
            expect(food['pickupTime']).to eq(food_attr[:pickup_time])
            expect(DateTime.parse(food['expDate']).strftime('%Y-%m-%d')).to eq(food_attr[:exp_date])
            expect(food['location']['id']).to be_present
            expect(food['location']['placeId']).to eq(food_attr[:location][:place_id])
            expect(food['location']['lat']).to be_within(0.000001).of(food_attr[:location][:lat])
            expect(food['location']['lng']).to be_within(0.000001).of(food_attr[:location][:lng])
            expect(food['location']['formattedAddress']).to eq(food_attr[:location][:formatted_address])
            expect(food['labels']).to be_kind_of(Array)
            expect(food['labels'][0]['id']).to be_present
            expect(food['labels'][0]['name']).to eq(label.name)
            expect(food['allergens']).to be_kind_of(Array)
            expect(food['allergens'][0]['id']).to be_present
            expect(food['allergens'][0]['name']).to eq(allergen.name)
            expect(food['images'][0]['image']['id']).to be_present
            expect(food['images'][0]['large']['url']).to be_present
            expect(food['user']['id']).to eq(user.id.to_s)
            expect(food['listedAt']).to be_nil
          end
        end

        context 'when food is expired' do
          it 'does not create food' do
            label = create(:label)
            allergen = create(:allergen)
            location = build(:location)
            user = create(:user)
            image = create(:image, :food)
            food_attr = {
              title: 'foo',
              description: 'foo description',
              food_type: 'UNPREPARED',
              quantity: 2,
              pickup_time: 'anytime',
              location: location,
              exp_date: DateTime.now.strftime('%Y-%m-%d'),
              labels_ids: [label.id],
              allergens_ids: [allergen.id],
              images_ids: [image.id],
              status: 'UNLISTED'
            }
            post '/graphql', params: { query: save_food_mutation({ food: food_attr }) }, headers: get_auth_header_from(user)
            json = JSON.parse(response.body)
            expect(json['data']['saveFood']).to be_nil
            expect(json['errors'][0]['extensions']['code']). to eq('cannot_list_expired_food')
          end
        end
      end

      context 'when id is given' do
        context 'when valid input is given' do
          it 'updates the food with given id' do
            user = create(:user)
            food = create(:food, user: user)
            label = create(:label)
            allergen = create(:allergen)
            location = build(:location)
            image = create(:image, :food)
            food_attr = {
              id: food.id,
              title: 'foo',
              description: 'foo description',
              food_type: 'UNPREPARED',
              quantity: 2,
              pickup_time: 'anytime',
              location: location,
              exp_date: 10.days.from_now.strftime('%Y-%m-%d'),
              labels_ids: [label.id],
              allergens_ids: [allergen.id],
              images_ids: [image.id],
              status: 'UNLISTED'
            }
            post '/graphql', params: { query: save_food_mutation({ food: food_attr }) }, headers: get_auth_header_from(user)
            json = JSON.parse(response.body)
            food = json['data']['saveFood']['food']
            expect(food['id']).to be_present
            expect(food['title']).to eq(food_attr[:title])
            expect(food['description']).to eq(food_attr[:description])
            expect(food['foodType']).to eq(food_attr[:food_type])
            expect(food['quantity']).to eq(food_attr[:quantity])
            expect(food['pickupTime']).to eq(food_attr[:pickup_time])
            expect(DateTime.parse(food['expDate']).strftime('%Y-%m-%d')).to eq(food_attr[:exp_date])
            expect(food['location']['id']).to be_present
            expect(food['location']['placeId']).to eq(food_attr[:location][:place_id])
            expect(food['location']['lat']).to be_within(0.000001).of(food_attr[:location][:lat])
            expect(food['location']['lng']).to be_within(0.000001).of(food_attr[:location][:lng])
            expect(food['location']['formattedAddress']).to eq(food_attr[:location][:formatted_address])
            expect(food['labels']).to be_kind_of(Array)
            expect(food['labels'].length).to eq(1)
            expect(food['labels'][0]['id']).to be_present
            expect(food['labels'][0]['name']).to eq(label.name)
            expect(food['allergens']).to be_kind_of(Array)
            expect(food['labels'].length).to eq(1)
            expect(food['allergens'][0]['id']).to be_present
            expect(food['allergens'][0]['name']).to eq(allergen.name)
            expect(food['images'][0]['image']['id']).to be_present
            expect(food['images'][0]['large']['url']).to be_present
            expect(food['user']['id']).to eq(user.id.to_s)
            expect(food['listedAt']).to be_present
          end

          it('sets listedAt') do
            user = create(:user)
            food = create(:food, :unlisted, user: user)
            expect(food.listed_at).to be_nil
            label = create(:label)
            allergen = create(:allergen)
            location = build(:location)
            image = create(:image, :food)
            food_attr = {
              id: food.id,
              title: 'foo',
              description: 'foo description',
              food_type: 'UNPREPARED',
              quantity: 2,
              pickup_time: 'anytime',
              location: location,
              exp_date: 10.days.from_now.strftime('%Y-%m-%d'),
              labels_ids: [label.id],
              allergens_ids: [allergen.id],
              images_ids: [image.id],
              status: 'LISTED'
            }
            post '/graphql', params: { query: save_food_mutation({ food: food_attr }) }, headers: get_auth_header_from(user)
            food.reload
            expect(food.listed_at).to be_present
          end
        end
      end
    end
  end
end
