# frozen_string_literal: true

require 'rails_helper'
require 'sidekiq/testing'

RSpec.describe FoodNotificationWorker, type: :worker do
  before do
    Sidekiq::Worker.clear_all
  end

  it 'sends new listing notification' do
    described_class.perform_async('ONCE_A_DAY')
    expect(described_class.jobs.size).to eq 1
  end
end
