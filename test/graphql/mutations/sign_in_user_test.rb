# frozen_string_literal: true

require 'test_helper'

module Mutations
  module Users
    class SignInTest < ActiveSupport::TestCase
      def sign_up(args = {})
        Mutations::CreateUser.new(object: nil, field: nil, context: { session: {} }).resolve(args)
      end

      def sign_in(args = {})
        Mutations::SignInUser.new(object: nil, field: nil, context: { session: {} }).resolve(args)
      end

      test 'should successfully login users by email' do
        gql_resp = sign_up(
          credentials: {
            provider: 'EMAIL',
            email: 'gmail@chucknorris.com',
            first_name: 'Chuck',
            last_name: 'Norris',
            password: '12345678'
          }
        )
        user = gql_resp[:user]
        assert user.present?
        assert user[:id].present?
        gql_resp = sign_in(
          credentials: {
            provider: 'EMAIL',
            email: user[:email],
            password: '12345678'
          }
        )
        user = gql_resp[:user]
        token = gql_resp[:token]
        assert user[:id].present?
        assert user[:first_name] = 'Chuck'
        assert token.present?
      end

      test 'should successfully do social login' do
        sign_up(
          credentials: {
            provider: 'FACEBOOK',
            email: 'gmail@chucknorris.com',
            access_token: '1000000000000001',
            first_name: 'Chuck',
            last_name: 'Norris',
            password: '12345678'
          }
        )
        gql_resp_1 = sign_in(
          credentials: {
            provider: 'FACEBOOK',
            access_token: '1000000000000001'
          }
        )
        gql_resp_2 = sign_in(
          credentials: {
            provider: 'EMAIL',
            email: 'gmail@chucknorris.com',
            password: '12345678'
          }
        )
        [gql_resp_1, gql_resp_2].each do |gql_resp|
          user = gql_resp[:user]
          token = gql_resp[:token]
          assert user[:id].present?
          assert user[:email] = 'gmail@chucknorris.com'
          assert user[:last_name] = 'Norris'
          assert token.present?
        end
      end

      test 'should not allow invalid login' do
        sign_up(
          credentials: {
            provider: 'FACEBOOK',
            email: 'gmail@chucknorris.com',
            access_token: '1000000000000001',
            first_name: 'Chuck',
            last_name: 'Norris',
            password: '12345678'
          }
        )
        gql_resp = sign_in(
          credentials: {
            provider: 'EMAIL',
            email: 'gmail@chucknorris.com-wrong',
            password: '12345678'
          }
        )
        assert gql_resp[:user].blank?
        assert gql_resp[:token].blank?
      end

      test 'should not allow disabled or deleted users' do
        user = sign_up(
          credentials: {
            provider: 'FACEBOOK',
            email: 'gmail@chucknorris.com',
            access_token: '1000000000000001',
            first_name: 'Chuck',
            last_name: 'Norris',
            password: '12345678'
          }
        )[:user]
        gql_resp = sign_in(
          credentials: {
            provider: 'EMAIL',
            email: 'gmail@chucknorris.com',
            password: '12345678'
          }
        )
        user = gql_resp[:user]
        token = gql_resp[:token]
        assert user[:id].present?
        assert user[:email] = 'gmail@chucknorris.com'
        assert user[:last_name] = 'Norris'
        assert token.present?

        user = User.find_by email: 'gmail@chucknorris.com'
        user.status = 'DELETED'
        user.save!

        gql_resp = sign_in(
          credentials: {
            provider: 'EMAIL',
            email: 'gmail@chucknorris.com',
            password: '12345678'
          }
        )
        assert gql_resp[:user].blank?
        assert gql_resp[:token].blank?
      end
    end
  end
end
