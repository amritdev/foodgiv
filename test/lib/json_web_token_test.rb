# frozen_string_literal: true

require 'test_helper'

class JsonWebTokenTest < ActiveSupport::TestCase
  test 'should be a string' do
    token = JsonWebToken.encode(1_993_619)
    assert token.present?
    assert token.is_a? String
  end
end
