#!/bin/bash
set -e

# Remove a potentially pre-existing server.pid for Rails.
rm -f /app/tmp/pids/server.pid

# Uncomment these commands for the first run
# bin/rails db:create
# bin/rails db:migrate RAILS_ENV=development
# bin/rails db:seed
# if [[ $? != 0 ]]; then
#   echo
#   echo "== Failed to migrate. Running setup first."
#   echo
#   bin/rails db:setup
# fi

if [ "$1" = 'redis-server' -a "$(id -u)" = '0' ]; then
    find . \! -user redis -exec chown redis '{}' +
    exec gosu redis "$0" "$@"
fi

# Then exec the container's main process (what's set as CMD in the Dockerfile).
exec "$@"