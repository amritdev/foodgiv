FROM ruby:2.6.5

RUN apt-get update && apt-get install -y \
  build-essential \
  default-mysql-client \
  vim \
  nodejs \
  imagemagick \
  libvips 

RUN mkdir /app
WORKDIR /app

COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
RUN bundle install

COPY . /app

# Add a script to be executed every time the container starts.
COPY docker-entrypoint.sh /usr/bin
RUN chmod +x /usr/bin/docker-entrypoint.sh
ENTRYPOINT [ "sh", "./docker-entrypoint.sh" ]
EXPOSE 3000

# Start the main process.
CMD [ "rails", "server", "-b", "0.0.0.0" ]